/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#ifndef MAPSCENE_H
#define MAPSCENE_H

#include <QColor>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>
#include <QGraphicsSceneMouseEvent>

class FedMap;
class Tiles;

class MapScene : public QGraphicsScene
{
private:
	int height;
	int width;
	
	QColor	background_color;

	FedMap	*fed_map;
	Tiles		*tiles;

public:
	MapScene(FedMap *f_map,QObject *parent = 0);
	~MapScene();

	QColor			GetBgColor();
	QGraphicsItem	*RemoveCurrentMarker(int abs_row,int abs_column);

	void	AddTile(int abs_row,int abs_column,int tile_num);
	void	AddCurrentMarker(int abs_row,int abs_column,QGraphicsItem *item);
	void	ChangeTile(int abs_row,int abs_column,int tile_num,const std::string& name);
	void	InitialiseMap();
	void	SetBgColor(const QColor& color);
};


#endif

