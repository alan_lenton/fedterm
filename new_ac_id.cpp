/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#include "new_ac_id.h"

#include <cctype>
#include <string>

#include "fedterm.h"


NewAcId::NewAcId(QWidget *parent) : QDialog(parent)
{
	ui.setupUi(this);

	ui.id_edit->clear();

	connect(ui.retry_btn,SIGNAL(clicked()),this,SLOT(RetryAccountId()));
	connect(ui.cancel_btn,SIGNAL(clicked()),this,SLOT(CancelConnection()));

}

NewAcId::~NewAcId()
{

}

void	NewAcId::CancelConnection()
{
	FedTerm::MainWindow()->LogOff();
	hide();
}

void	NewAcId::RetryAccountId()
{
	if(!AccountIdOK())
		return;
	QString	text(ui.id_edit->text());
	text += '\n';
	FedTerm::MainWindow()->SendCmd(text);
	hide();
}

bool NewAcId::AccountIdOK()
{
	static const QString	title = "New Account";

	if(ui.id_edit->text().size() < 3)
	{
		FedTerm::MainWindow()->Message(title,"Account Ids must be at least\nfive characters long!");
		return false;
	}

	std::string text(ui.id_edit->text().toStdString());
	for(int count = 0;count < text.length();++count) 
	{
		if((std::isalpha(text[count]) == 0) || (text[count] == ' '))
		{
			FedTerm::MainWindow()->Message(title,"Account Ids must be all letters\nor characters with no spaces!");
			return false;
		}
	}

	return true;
}

