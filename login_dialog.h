/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include <memory>

#include <QDialog>
#include <QImage>
#include <QString>

#include "ui_login_dialog.h"


struct LoginRec
{
	QString	host;
	quint16	port;
	QString	avatar_name;
	QString	account_id;
	QString	password;
	bool		save_pwd;
};

class LoginDialog : public QDialog
{
	Q_OBJECT

private:
	static const QString add_avatar;

	Ui::LoginDialog ui;

	LoginRec	*login_rec;
	QImage	picture;

	bool		eventFilter(QObject *target, QEvent *event);
	void		LoadDetails(const QString& current_avatar);

private slots:
	void	AvatarChanged(const QString& new_name);
	void	Cancel();
	void	Connect();
	void	DeleteRec();

signals:
	void	LoginInfoReady();

public:
	LoginDialog(LoginRec *rec,QWidget *parent = 0);
	~LoginDialog();
};

#endif
