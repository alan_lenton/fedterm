/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/
#include "newbie.h"

#include <string>

#include <cctype>
#include <QPainter>

#include "fedterm.h"

const QString	Newbie:: msg_title("New Player");

Newbie::Newbie(QWidget *parent) : QDialog(parent)
{
	ui.setupUi(this);

	newbie_image = QImage(":/FedTerm/Images/newbie.png");
	ui.newbie_pic->installEventFilter(this);
	unused_points = MAX_UNUSED_POINTS; 
	strength = dexterity = stamina = intelligence = MIN_STAT_SIZE;

	connect(ui.cancel_btn,SIGNAL(clicked()),this,SLOT(Cancel()));
	connect(ui.send_to_server_btn,SIGNAL(clicked()),this,SLOT(Connect()));
	
	connect(ui.strength_spin,SIGNAL(valueChanged(int)),this,SLOT(StrChanged(int)));
	connect(ui.dex_spin,SIGNAL(valueChanged(int)),this,SLOT(DexChanged(int)));
	connect(ui.stamina_spin,SIGNAL(valueChanged(int)),this,SLOT(StamChanged(int)));
	connect(ui.intell_spin,SIGNAL(valueChanged(int)),this,SLOT(IntChanged(int)));
}

Newbie::~Newbie()
{

}


bool	Newbie::AccountValid()
{
	if((ui.email_edit->text().size() < 5) || (ui.email_edit->text().indexOf('@') < 0)) // a@b.c
	{
		FedTerm::MainWindow()->Message(msg_title,"You must provide a valid email address!");
		return false;
	}

	if(ui.ac_id_edit->text().size() < 5)
	{
		FedTerm::MainWindow()->Message(msg_title,"Account names must be at\nleast five characters long!");
		return false;
	}
	std::string id_text(ui.ac_id_edit->text().toStdString());
	for(int count = 0;count < id_text.length();++count) 
	{
		if((std::isalpha(id_text[count]) == 0) || (id_text[count] == ' '))
		{
			FedTerm::MainWindow()->Message(msg_title,"Account IDs must be all letters\nor characters with no spaces!");
			return false;
		}
	}

	if(ui.pw_edit->text().size() < 8)
	{
		FedTerm::MainWindow()->Message(msg_title,"Passwords must be at least\neight characters long!");
		return false;
	}
	if(ui.pw_edit->text() != ui.pw_confirm_edit->text())
	{
		FedTerm::MainWindow()->Message(msg_title,"The password and its\nconfirmation don't match!");
		return false;
	}

	return true;
}

bool	Newbie::AvatarValid()
{
	if(ui.avatar_name_edit->text().size() < 3)
	{
		FedTerm::MainWindow()->Message(msg_title,"Avatar names must be at least\nthree characters long!");
		return false;
	}
	std::string text(ui.avatar_name_edit->text().toStdString());
	for(int count = 0;count < text.length();++count) 
	{
		if((std::isalpha(text[count]) == 0) || (text[count] == ' '))
		{
			FedTerm::MainWindow()->Message(msg_title,"Avatar names must be all\nletters with no spaces!");
			return false;
		}
	}

	if(ui.species_edit->text().size() < 4)
	{
		FedTerm::MainWindow()->Message(msg_title,"Species names must be at least\nfour characters long!");
		return false;
	}

	return true;
}

void	Newbie::Cancel()	{ hide(); }

void	Newbie::Connect()
{
	if(!DetailsValid())
		return;
	
	rec.server_name = ui.server_name_edit->text();
	rec.port_num = ui.port_num_edit->text().toInt();
	
	rec.account_id = ui.ac_id_edit->text();
	rec.pwd = ui.pw_edit->text();
	rec.email = ui.email_edit->text();
	rec.save_pw = ui.pw_save->isChecked();

	rec.name = ui.avatar_name_edit->text();
	rec.race = ui.species_edit->text();
	if(ui.male_btn->isChecked())
		rec.gender = "male";
	else
	{
		if(ui.female_btn->isChecked())
			rec.gender = "female";
		else
			rec.gender = "neuter";
	}
	rec.strength = ui.strength_spin->value();
	rec.stamina = ui.stamina_spin->value();
	rec.dexterity = ui.dex_spin->value();
	rec.intelligence = ui.intell_spin->value();

	emit NewbieInfoReady();
}

bool	Newbie::DetailsValid()
{
// The following are checked  via limits set in the newbie.ui form:
//		Account ID max is 23
//		Password/Pw Confirm max is 15
//		Avatar name max is 15
//		Species max is 14
//		All stats - min 20/max 70
//		Unused stats - min 0/max 60

	if(!ServerValid() || !AccountValid() || !AvatarValid())
		return false;
	else
		return true;
}
 
void	Newbie::DexChanged(int new_val)
{
	if(UnusedStatsChanged(dexterity - new_val))
		dexterity = new_val;
	else
		ui.dex_spin->setValue(dexterity);
}

bool Newbie::eventFilter(QObject *target, QEvent *event)
{
	if(event->type() == QEvent::Paint)
	{
		if(target == ui.newbie_pic)
		{
			QPainter	painter(ui.newbie_pic);
			painter.drawImage(0,0,newbie_image);
			return true;
		}
	}
	return(QWidget::eventFilter(target, event));
}

void	Newbie::IntChanged(int new_val)
{
	if(UnusedStatsChanged(intelligence - new_val))
		intelligence = new_val;
	else
		ui.intell_spin->setValue(intelligence);
}

bool	Newbie::ServerValid()
{
	if(ui.server_name_edit->text().size() < 3)	// a.b
	{
		FedTerm::MainWindow()->Message(msg_title,"You need a valid internet\naddress for the server!");
		return false;
	}
	if(ui.port_num_edit->text().toInt() <= 0)
	{
		FedTerm::MainWindow()->Message(msg_title,"The port number must be positive\nand greater than zero!");
		return false;
	}
	return true;
}

void	Newbie::StamChanged(int new_val)
{
	if(UnusedStatsChanged(stamina - new_val))
		stamina = new_val;
	else
		ui.stamina_spin->setValue(stamina);
}

void	Newbie::StrChanged(int new_val)
{
	if(UnusedStatsChanged(strength - new_val))
		strength = new_val;
	else
		ui.strength_spin->setValue(strength);
}

bool	Newbie::UnusedStatsChanged(int increment)
{
	static QString	label = "Unused ";
	unused_points += increment;
	if(unused_points < 0)
	{
		unused_points = 0;
		return false;
	}

	ui.unused_stats_label->setText(label + QString::number(unused_points));
	return true;
}

