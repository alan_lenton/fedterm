/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#ifndef NEWBIE_H
#define NEWBIE_H

#include <QDialog>

#include "ui_newbie.h"

struct NewbieRec
{
	QString	server_name;
	int		port_num;

	QString	account_id;
	QString	pwd;
	QString	email;
	bool		save_pw;

	QString	name;
	QString	race;
	QString	gender;
	int		strength;
	int		stamina;
	int		dexterity;
	int		intelligence;
};

class Newbie : public QDialog
{
	Q_OBJECT

public:
	const int	MIN_ID_SIZE = 5;
	const int	MIN_NAME_SIZE = 3;
	const int	MIN_PW_SIZE = 8;
	const int	MIN_STAT_SIZE = 20;
	const int	MAX_UNUSED_POINTS = 60;

	enum { STR, DEX, STAM, INT };

private:
	static const QString	msg_title;

	Ui::newbie	ui;

	QImage		newbie_image;
	NewbieRec	rec;
	int	unused_points;
	int	strength;
	int	dexterity;
	int	stamina;
	int	intelligence;

	bool	eventFilter(QObject *target, QEvent *event);
	
	bool	AccountValid();
	bool	AvatarValid();
	bool	DetailsValid();
	bool	ServerValid();
	bool	UnusedStatsChanged(int increment);

private slots:
	void	Cancel();
	void	Connect();
	void	DexChanged(int new_val);
	void	IntChanged(int new_val);
	void	StrChanged(int new_val);
	void	StamChanged(int new_val);

signals:
	void	NewbieInfoReady();

public:
	Newbie(QWidget *parent = 0);
	~Newbie();

	NewbieRec *GetNewbieRec()	{ return &rec; }
};

#endif
