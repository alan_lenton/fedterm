/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#ifndef EXCHANGE_H
#define EXCHANGE_H

#include <string>

#include "xml_parser.h"

class ServerParser;

class Exchange
{
public:
	static const int	TABLE_ROWS = 73;
	static const int	TABLE_COLS = 3;
	static const std::string commodity_names[];
	enum { BUY, SELL };

private:
	const int	NO_INDEX = -1;
	std::string	name;
	std::string	commod_name;
	std::string	commod_stock;
	std::string	commod_price;

	std::string	old_commod_name;
	std::string old_display;

	int	FindCurrentRow();

public:
	Exchange(const AttribList& attribs,ServerParser *parser);
	~Exchange()	{	}

	void	Transaction(const AttribList& attribs,ServerParser *parser,int type);
	void	CargoManifest(const AttribList& attribs,ServerParser *parser);
	void	CargoLine(const AttribList& attribs,ServerParser *parser);
};

#endif
