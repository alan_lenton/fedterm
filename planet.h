/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#ifndef PLANET_H
#define PLANET_H

#include <string>

#include "xml_parser.h"

class ServerParser;

class Planet
{
private:
	enum InfraStates	{ NONE, WAREHOUSES, DEPOTS, FACTORIES };

	std::string	name;
	std::string	star;
	std::string cartel;
	std::string	owner;
	
	std::string	economy;
	std::string	total_wf;
	std::string	available_wf;
	std::string	yard;
	std::string	fleet;

	std::string	approval;

	InfraStates	infra_state;

public:
	Planet(const AttribList& attribs,ServerParser *parser);
	~Planet() {	}

	void	Depots(const AttribList& attribs,ServerParser *parser);
	void	Factories(const AttribList& attribs,ServerParser *parser);
	void	InfraBuilds(const AttribList& attribs,ServerParser *parser);
	void	Warehouses(const AttribList& attribs,ServerParser *parser);
};
#endif

