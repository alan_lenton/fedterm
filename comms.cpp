/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#include "comms.h"

#include <sstream>

#include <QMessageBox>

#include "fedterm.h"
#include "login_dialog.h"
#include "newbie.h"
#include "server_parser.h"
#include "stat_table.h"

Comms::Comms(QObject *parent) : QObject(parent)
{
	server_buffer = new char[SERVER_BUFFER_SIZE];
	tcp_socket = new QTcpSocket(this);
	parser = new ServerParser();
	login_rec = nullptr;
	newbie_rec = nullptr;
	status = Status::NO_CONNECT;
	name_taken = false;

	// Socket connections
	connect(tcp_socket,SIGNAL(connected()),this,SLOT(Connected()));
	connect(tcp_socket,SIGNAL(disconnected()),this,SLOT(ConnectionClosed()));
	connect(tcp_socket,SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(CommsError(QAbstractSocket::SocketError)));
	connect(tcp_socket,SIGNAL(readyRead()),this,SLOT(ParseIncoming()));
}

Comms::~Comms()
{
	if(tcp_socket->state() == QAbstractSocket::ConnectedState)
		tcp_socket->close();
	delete tcp_socket;
	delete[]	server_buffer;
}

void	Comms::CommsError(QAbstractSocket::SocketError error)
{
	static const QString	contact("Unable to contact the server! ("); 
	static const QString	network("Unable to set up a network connection! (");
	static const QString	default_error("Connection to server lost! (");

	switch(static_cast<int>(error))
	{
		case  0:
		case  1:	return;	// Not an error...
		case  2:	FedTerm::MainWindow()->Message("Communications",contact + QString::number(error) + ')');			break;
		case  3:
		case  4:	
		case  5:
		case  6:
		case  7:
		case  8:
		case  9:
		case 10:	FedTerm::MainWindow()->Message("Communications",network + QString::number(error) + ')');			break;
		default:	FedTerm::MainWindow()->Message("Communications",default_error + QString::number(error) + ')');	break;
	}
	LogOff();
}

void	Comms::ConnectToServer(LoginRec *rec)
{
	newbie_rec = nullptr;
	login_rec = rec;
	tcp_socket->connectToHost(login_rec->host,rec->port);
}

void	Comms::ConnectToServer(NewbieRec *rec)
{
	login_rec = nullptr;
	newbie_rec = rec;
	tcp_socket->connectToHost(newbie_rec->server_name,rec->port_num);
}

void	Comms::Connected()
{
	FedTerm::MainWindow()->SetConnectDisplay();
	if(login_rec != nullptr)
		status = Status::LOGIN;
	else
		status = Status::NEWBIE_LOGIN;
}

void	Comms::ConnectionClosed()
{
	tcp_socket->close();
	parser->Reset();
	FedTerm::MainWindow()->ConnectionClosed();

}

void	Comms::Login(std::string& buffer)
{
	if(buffer.find("Login:") != std::string::npos)
	{
		std::string	temp(login_rec->account_id.toStdString());
		temp += "\n";
		Write(temp.c_str());
		return;
	}
	
	if(buffer.find("account name or password is wrong.") != std::string::npos)
	{
		FedTerm::MainWindow()->LogOff();
		FedTerm::MainWindow()->Message("Login","Account name or password is wrong.\nPlease re-login with the correct details.");
		FedTerm::MainWindow()->HideLoginDialog();
	}

	if(buffer.find("Password:") != std::string::npos)
	{
		std::string temp(login_rec->password.toStdString());
		temp += "\n";
		Write(temp.c_str());
		return;
	}

	if(buffer.find("Linking") != std::string::npos)
	{
		FedTerm::MainWindow()->SaveLoginRec();
		status = Status::START_XML;
		return;
	}

	// Must be intro stuff - print it to the output window 
	FedTerm::MainWindow()->DisplayMain(buffer);
}

void	Comms::NewbieLogin(std::string& buffer)
{
	FedTerm::MainWindow()->DisplayMain(buffer);

	if(buffer.find("Login:") != std::string::npos)
	{
		Write("new\n");
		return;
	}

	if(buffer.find("name for your account") != std::string::npos)
	{
		std::string	temp(newbie_rec->account_id.toStdString());
		temp += "\n";
		Write(temp.c_str());
		return;
	}

	// Note: this handles both the password and confirmation
	if(buffer.find("password") != std::string::npos)
	{
		std::string temp(newbie_rec->pwd.toStdString());
		temp += "\n";
		Write(temp.c_str());
		return;
	}

	if(buffer.find("e-mail") != std::string::npos)
	{
		std::string temp(newbie_rec->email.toStdString());
		temp += "\n";
		Write(temp.c_str());
		return;
	}

	if(buffer.find("provide a name for your character") != std::string::npos)
	{
		std::string	temp(newbie_rec->name.toStdString());
		temp += "\n";
		Write(temp.c_str());
		return;
	}

	if(buffer.find("race of your character") != std::string::npos)
	{
		std::string	temp(newbie_rec->race.toStdString());
		temp += "\n";
		Write(temp.c_str());
		return;
	}

	if(buffer.find("gender of your character") != std::string::npos)
	{
		std::string	temp(newbie_rec->gender.toStdString());
		temp += "\n";
		Write(temp.c_str());
		return;
	}

	if(buffer.find("stats equally") != std::string::npos)
	{
		Write("no\n");
	return;
	}

	if(buffer.find("allocate to strength?") != std::string::npos)
	{
		std::ostringstream	temp;
		temp << newbie_rec->strength << "\n";
		Write(temp.str().c_str());
		return;
	}

	if(buffer.find("allocate to stamina?") != std::string::npos)
	{
		std::ostringstream	temp;
		temp << newbie_rec->stamina << "\n";
		Write(temp.str().c_str());
		return;
	}

	if(buffer.find("allocate to dexterity?") != std::string::npos)
	{
		std::ostringstream	temp;
		temp << newbie_rec->dexterity << "\n";
		Write(temp.str().c_str());
		return;
	}

	if(buffer.find("want to play") != std::string::npos)
	{
		Write("yes\n");
		return;
	}

	if(buffer.find("that name is not available") != std::string::npos)
	{
		FedTerm::MainWindow()->LogOff();
		FedTerm::MainWindow()->Message("Login","That avatar name is not available for use.\nPlease try again with a different name.");
		FedTerm::MainWindow()->HideNewbieDialog();
	}

	if(buffer.find("That account name is already in use") != std::string::npos)
	{
		FedTerm::MainWindow()->LogOff();
		FedTerm::MainWindow()->Message("Login","That account name is not available for use.\nPlease try again with a different account name.");
		FedTerm::MainWindow()->HideNewbieDialog();
	}
	
	if(buffer.find("Linking") != std::string::npos)
	{
		FedTerm::MainWindow()->SaveLoginRec(*newbie_rec);
		status = Status::START_XML;
		return;
	}
}

void	Comms::ParseIncoming()
{
	std::string	buffer;
	while(tcp_socket->canReadLine())
	{
		tcp_socket->readLine(server_buffer,SERVER_BUFFER_SIZE);
		buffer = server_buffer;

		if(status == Status::PLAY)
		{
			parser->Parse(buffer);
			continue;
		}

		if(status == Status::LOGIN)
			Login(buffer);
		if(status == Status::START_XML)
			StartXml(buffer);
		if(status == Status::WAIT_XML)
			WaitXml(buffer);
		if(status == Status::NEWBIE_LOGIN)
			NewbieLogin(buffer);
	}
}

void	Comms::ShowFriends()			{ parser->ShowFriends(); }

void	Comms::StartXml(std::string& buffer)
{
	Write("<?xml version=\"1.0\"?>\n");

	std::string	temp("<c-fedterm version='");
	temp += FedTerm::version;
	temp += "'>\n";
	Write(temp.c_str());

	temp = "<c-comms-level level='";
	temp += FedTerm::comms_level;
	temp += "'>\n";
	Write(temp.c_str());
	
	status = Status::WAIT_XML;
}

void	Comms::WaitXml(std::string& buffer)
{
	if(buffer.find("<s-fedterm/>") != std::string::npos)
	{
		parser->SetMap(FedTerm::MainWindow()->GetMap());
		status = Status::PLAY;
		Write("gl\n");		// Needed to force placement of 'You are here marker' on map

		QString	start_macro = FedTerm::MainWindow()->StartMacro();
		if(!start_macro.isEmpty())
		{
			FedTerm::MainWindow()->DisplayMain(start_macro.toStdString());
			Write(start_macro);
		}
		FedTerm::MainWindow()->SetInputFocus();
	}
	else
		FedTerm::MainWindow()->DisplayMain(buffer);
}

void	Comms::WantTextAlerts(bool which)	{ parser->WantTextAlerts(which); }

void	Comms::Write(const QString& text)
{
	tcp_socket->write(text.toStdString().c_str());
}

void	Comms::Write(const char *text)
{
	tcp_socket->write(text);
}



