/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#ifndef MAPPARSER_H
#define MAPPARSER_H

#include <QFile>
#include <QLabel>
#include <QString>

#include <QXmlDefaultHandler>

class Location;
class FedMap;

class MapParser : public QXmlDefaultHandler
{
private:
	static const int		NOT_FOUND = 999;

	FedMap	*fed_map;
	Location	*location;
	QFile		*file;
	QString	file_name;
	QString	text;

	// These two functions are overrides of virtuals in QXmlDefaultHandler
	bool	endElement(const QString& namespaceURI,const QString& localName,const QString& name) override;
	bool	startElement(const QString& namespaceURI,const QString& localName,
														const QString& qName, const QXmlAttributes& atts) override;

	int	FindElement(const QString& element);
	void	StartLocation(const QXmlAttributes& attribs);

public:
	MapParser(const std::string f_name, FedMap *the_map);
	~MapParser();

	bool	Parse();
};

#endif
