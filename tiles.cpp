/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#include "tiles.h"

const QString Tiles::pix_files[] = 
{
	":/FedTerm/Images/unused.png",		":/FedTerm/Images/used.png",	
	":/FedTerm/Images/selected.png",		":/FedTerm/Images/bad.png",		
	":/FedTerm/Images/used-hidden.png",	":/FedTerm/Images/north-arrow.png",	
	":/FedTerm/Images/ne-arrow.png",		":/FedTerm/Images/east-arrow.png",
	":/FedTerm/Images/se-arrow.png",		":/FedTerm/Images/south-arrow.png",	
	":/FedTerm/Images/sw-arrow.png",		":/FedTerm/Images/west-arrow.png",		
	":/FedTerm/Images/nw-arrow.png",		":/FedTerm/Images/current.png",
	":/FedTerm/Images/link.png",			":/FedTerm/Images/peace.png"
};

Tiles::Tiles()
{
	for(auto count = 0;count < NUM_TILES;++count)
		pix_maps[count] = new QPixmap(pix_files[count]);
}

Tiles::~Tiles()
{
	for(auto count = 0;count < NUM_TILES;++count)
		delete pix_maps[count];
}
