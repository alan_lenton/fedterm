/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#include "server_parser.h"

#include "avatar.h"
#include "exchange.h"
#include "fed_map.h"
#include "fedterm.h"
#include "friends.h"
#include "planet.h"
#include "soundfx.h"

const std::string	ServerParser::elements[] = 
{
	"s-fedterm",				"s-default",					"s-add-player",			//  0-> 2
	"s-remove-player",		"s-player-stats",				"s-ship-stats",			//  3-> 5
	"s-new-loc",				"s-new-map",					"s-link",					//  6-> 8
	"s-planet-name",			"s-gen-planet-info",			"s-ware-planet-info",	//  9->11
	"s-depot-planet-info",	"s-factory-planet-info",	"s-build-planet-info",	// 12->14
	"s-exchange",				"s-exch-sell",					"s-exch-buy",				// 15->17
	"s-sound",					"s-manifest",					"s-cargo",					// 18->20 
	"s-computer-stats",		"s-weapon-stats",				"s-spynet-start",			//	21->23
	"s-examine-start",		"s-message",					"s-full-stats",			// 24->26
	"s-weapons",
	""
};

ServerParser::ServerParser()
{
	avatar = new Avatar();
	friends = new Friends;

	fed_map = nullptr;
	planet = nullptr;
	exchange = nullptr;
	want_text_alerts = false;	// Fedterm class still being constructed at this point

	status = CLEAR;
}

ServerParser::~ServerParser()
{
	delete avatar;
	delete planet;
}


void	ServerParser::AddPlanet(const AttribList& attribs)
{
	std::string	name;
	FindAttrib(attribs,"name", name);
	if(name != "")
		FedTerm::MainWindow()->AddPlanet(name);
	status = ADD_PLANET;
}

void	ServerParser::AddPlayer(const AttribList& attribs)
{
	std::string	name;
	FindAttrib(attribs,"name", name);
	if(name != "")
	{
		FedTerm::MainWindow()->AddPlayer(name);
		int rank = FindNumAttrib(attribs,"rank",-1);
		friends->Notify(name,rank,Friends::LOGIN);
	}
	status = ADD_PLAYER;
}

void	ServerParser::AddSystem(const AttribList& attribs)
{
	std::string	name;
	FindAttrib(attribs,"name", name);
	if(name != "")
		FedTerm::MainWindow()->AddSystem(name);
	status = ADD_SYSTEM;
}

void	ServerParser::EndElement(const std::string& elem)
{
	status = CLEAR;
}

void	ServerParser::ExchangeBuy(const AttribList& attribs)
{
	if(exchange != nullptr)
	{
		exchange->Transaction(attribs,this,Exchange::BUY);
		status = EXCH_BUY;
	}
}

void	ServerParser::ExchangeSell(const AttribList& attribs)
{
	if(exchange != nullptr)
	{
		exchange->Transaction(attribs,this,Exchange::SELL);
		status = EXCH_SELL;
	}
}

void	ServerParser::ExchangeStart(const AttribList& attribs)
{
	status = EXCH_START;
	if(exchange != nullptr)
		delete exchange;
	exchange = new Exchange(attribs,this);
	FedTerm::MainWindow()->Switch2Exchange();
}

int	ServerParser::FindElement(const std::string& elem)
{
	for(auto count = 0;elements[count] != "";++count)
	{
		if(elements[count] == elem)
			return count;
	}
	return NOT_AN_ELEMENT;
}

void	ServerParser::GeneralPlanetInfo(const AttribList& attribs)
{
	status = PLANET_INFO;
	if(planet != nullptr)
		delete planet;
	planet = new Planet(attribs,this);
}

void	ServerParser::Message()
{
	FedTerm::MainWindow()->PlaySound(SoundFx::ALERT);
	status = DEFAULT;
}

void	ServerParser::PlanetDepots(const AttribList& attribs)
{
	status = PLANET_DEPOT;
	if(planet != nullptr)
		planet->Depots(attribs,this);
}

void	ServerParser::PlanetFactories(const AttribList& attribs)
{
	status = PLANET_FACTORY;
	if(planet != nullptr)
		planet->Factories(attribs,this);
}

void	ServerParser::PlanetInfraBuilds(const AttribList& attribs)
{
	status = INFRA_BUILD;
	if(planet != nullptr)
		planet->InfraBuilds(attribs,this);
}

void	ServerParser::PlanetWarehouses(const AttribList& attribs)
{
	status = PLANET_WARE;
	if(planet != nullptr)
		planet->Warehouses(attribs,this);
}

void	ServerParser::RemovePlayer(const AttribList& attribs)
{
	std::string	name;
	FindAttrib(attribs,"name", name);
	if(name != "")
	{
		FedTerm::MainWindow()->RemovePlayer(name);
		friends->Notify(name,-1,Friends::LOGOUT);
	}
	status = REMOVE_PLAYER;
}

void	ServerParser::ShowFriends()	{ friends->show(); }

void	ServerParser::StartElement(const std::string& elem,const AttribList& attribs)
{
	switch(FindElement(elem))
	{
		case  0:	want_text_alerts = FedTerm::MainWindow()->WantsTextAlert(); break;
		case  1:	status = DEFAULT;								break;
		case  2:	AddPlayer(attribs);							break;
		case  3: RemovePlayer(attribs);						break;
		case  4: avatar->AvatarInfo(attribs);				break;
		case  5: avatar->ShipInfo(attribs);					break;
		case  6: fed_map->NewLoc(attribs);					break;
		case  7: fed_map->NewMap(attribs);					break;
		case  8:	AddSystem(attribs);							break;
		case  9:	AddPlanet(attribs);							break;
		case 10:	GeneralPlanetInfo(attribs);				break;
		case 11: PlanetWarehouses(attribs);					break;
		case 12:	PlanetDepots(attribs);						break;
		case 13:	PlanetFactories(attribs);					break;
		case 14:	PlanetInfraBuilds(attribs);				break;
		case 15:	ExchangeStart(attribs);						break;
		case 16:	ExchangeSell(attribs);						break;
		case 17:	ExchangeBuy(attribs);						break;
		case 18:														break;
		case 19:	exchange->CargoManifest(attribs,this);	break;
		case 20:	exchange->CargoLine(attribs,this);		break;
		case 21: avatar->ComputerInfo(attribs);			break;
		case 22:	avatar->WeaponInfo(attribs);				break;
		case 23:	// Drop through - code for spynet & examine is identical...
		case 24:	Spynet(attribs);								break;
		case 25:	Message();										break;
		case 26:	avatar->FullStats(attribs);				break;
		case 27: avatar->Weapons(attribs);					break;
	}
}

void	ServerParser::Spynet(const AttribList& attribs)
{
	std::string	name;
	FindAttrib(attribs,"name",name);
	if(name != "")
	{
		std::string	rank;
		FindAttrib(attribs,"rank",rank);
		rank += " ";
		FedTerm::MainWindow()->DisplayMain(rank + name);
		FedTerm::MainWindow()->UpdatePlayersPic(name);
	}
}

void	ServerParser::TextData(const std::string& text)
{
	if(text == "\n")
		return;
	switch(status)
	{
		case DEFAULT:	// TODO: Fix this so that the alert happens only on incoming messages
			FedTerm::MainWindow()->DisplayMain(text);
			break;
		default:		FedTerm::MainWindow()->DisplayMain(text);	break;
	}
}


