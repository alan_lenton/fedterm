/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#ifndef STATTABLE_H
#define STATTABLE_H

#include <list>
#include <string>
#include <utility>

using AttribList = std::list<std::pair<std::string,std::string>>;

class Weapons;

class StatTable
{
public:
	const static int	NAME_COL  = 0;
	const static int	VALUE_COL = 1;
	static const int	UNKNOWN_ROW = -1;

	static void	MakeBiValueStat(const AttribList& attribs,std::string& text);

private:
	static const int	UNKNOWN_VAL = -1;

	// Avatar Stats
	static const std::string	xml_av_stat_names[];
	static const std::string	av_display_names[];
	enum { STA, STR, DEX, INTEL, CASH, PTS, HC, AK, TRADER, MERCHANT };

	std::string	stamina;
	std::string	strength;
	std::string	dexterity;
	std::string	intel;
	std::string	cash;
	std::string	av_points;
	std::string av_points_type;	// jobs/ak/mr/tp

	// Ship Stats
	static const std::string	xml_ship_stat_names[];
	static const std::string	ship_display_names[];
	enum { CLASS, FUEL, HULL, ENG, SHIELDS, CARGO, CUSTOMS };

	std::string	ship_class;
	std::string	fuel;
	std::string	hull;
	std::string	shields;
	std::string	engines;
	std::string	cargo;
	std::string	customs;

	// Computer Stats
	static const std::string	xml_comp_stat_names[];
	static const std::string	comp_display_names[];
	enum { COMPUTER, SENSORS, JAMMERS, NAVCOMP, };

	std::string	computer;
	std::string	sensors;
	std::string	jammers;
	std::string	navcomp;

	// Weapon stats handler
	Weapons	*weapons;

	bool	full_update;

	void	DisplayLine(const std::string& stat,const std::string& value,int row = UNKNOWN_ROW);
	void	EndFullUpdate();
	void	MakeCashStat(const AttribList& attribs,std::string& text);
	void	MakeRankStat(const AttribList& attribs,int which);
	void	StartFullUpdate();

public:
	StatTable();
	~StatTable();

	void	FullUpdate(const AttribList& attribs);
	void	ProcessAvatarInfo(const AttribList& attribs);
	void	ProcessShipInfo(const AttribList& attribs);
	void	ProcessComputerInfo(const AttribList& attribs);
	void	ProcessWeaponsInfo(const AttribList& attribs);
	void	WeaponsUpdate(const AttribList& attribs);
};

#endif
