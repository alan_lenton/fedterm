/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#include "stats.h"

#include <sstream>

#include "xml_parser.h"

void	Stats::MakeBiValueStat(const AttribList& attribs,std::string &text)
{
	int max = XmlParser::FindNumAttrib(attribs,"max",UNKNOWN_VAL);
	int current = XmlParser::FindNumAttrib(attribs,"cur",UNKNOWN_VAL);
	if((max == UNKNOWN_VAL) || (current == UNKNOWN_VAL))
	{
		text = "Unknown";
		return;
	}
	std::ostringstream	buffer;
	buffer << current << "/" << max;
	text = buffer.str();
}
