/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#include "logging.h"


#include <cstring>
#include <ctime>

#include "fedterm.h"

Logging::Logging()
{
	logging = comms_logging = false;
}

Logging::~Logging()
{
	if(logging)
		log_file.close();
	if(comms_logging)
		comms_file.close();
}


void	Logging::Log(const std::string& text)
{
	if((logging) && (log_file))
	{
		std::string	temp(text);
		int len = temp.length();
		if(temp[len - 1] !=  '\n')
			temp += "\n";
		log_file << temp;
	}
}

void	Logging::MakeBaseFileName(std::string& base_name)
{
	std::time_t	now = time(0);
	
	char	date[48], month[16], day[16], year[16];
	std::strcpy(date,std::ctime(&now));
	strtok(date," ");
	strcpy(month,strtok(0," "));
	strcpy(day,strtok(0," "));
	strtok(0," ");
	strcpy(year,strtok(0," \n"));

	base_name = std::string(year) + month + day;
}

bool Logging::StartLog()
{
	std::string	log_dir = FedTerm::base_dir + "/logs/";
	if(!FedTerm::MainWindow()->MakeDirectory(log_dir))
	{
		FedTerm::MainWindow()->Message(QString("Error"),QString("Unable to create directory for log files"));
		return false;
	}

	std::string base_name;
	MakeBaseFileName(base_name);
	std::string log_name = base_name + ".log";
	std::string	file_name = log_dir + log_name;
	log_file.open(file_name,std::ios::out | std::ios::app);
	if(!log_file)
	{
		FedTerm::MainWindow()->Message(QString::fromStdString(log_name), "Unable to open file for logging");
		return false;
	}

	std::time_t	now = time(0);
	log_file << "\n\n ====> NEW SESSION: " << std::ctime(&now) << std::endl;
	logging = true;	
	return true;
}

void	Logging::StopLogging()
{
	if(logging)
	{
		log_file.close();
		logging = false;
	}
}

