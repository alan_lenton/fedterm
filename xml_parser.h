 /*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#ifndef XMLPARSER_H
#define XMLPARSER_H

#include <list>
#include <string>
#include <utility>

// .first is the attribute name
// .second is the attribute's value
using AttribList = std::list<std::pair<std::string,std::string>>;

class XmlParser	// cut down parser
{
private:
	static std::string&	UnEscapeXml(std::string& text);

	std::string	element;
	AttribList	attrib_list;

	void	BuildElement(const std::string& line);
	void	BuildAttributes(const std::string& line);
	void	ProcessLine(const std::string& line);

protected:
	virtual void	StartElement(const std::string& elem,const AttribList& attribs)	{	}
	virtual void	EndElement(const std::string& elem)	{	}
	virtual void	TextData(const std::string& text)	{	}

public:
	// Parser utilities
	static std::string&	EscapeXml(const std::string& text);
	static void	FindAttrib(const AttribList& attribs,const std::string& name,std::string& value);
	static bool	FindBoolAttrib(const AttribList& attribs,const std::string& name,bool default_val);
	static int	FindNumAttrib(const AttribList& attribs,const std::string& name,int default_val);

	XmlParser();
	virtual ~XmlParser();

	void	Parse(const std::string& line);
};

#endif
