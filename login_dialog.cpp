     /*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#include "login_dialog.h"

#include <QMessageBox>
#include <QPainter>
#include <QSettings>

#include "fedterm.h"

const QString LoginDialog::add_avatar = "--Add an avatar--";

LoginDialog::LoginDialog(LoginRec *rec,QWidget *parent) : QDialog(parent)
{
	ui.setupUi(this);

	login_rec = rec;

	picture = QImage(":/FedTerm/Images/telescope1.png");
	ui.image_widget->installEventFilter(this);

	QSettings	settings(QSettings::IniFormat,QSettings::UserScope,"ibgames", "FedTerm");
	settings.beginGroup("Avatars");
	QString	current_avatar = settings.value("current_avatar",add_avatar).toString();
	if(current_avatar == add_avatar)	// First time through - no avatars in .ini file
	{
		ui.address_edit->setText("fed2.ibgames.net");
		ui.port_edit->setText("30003");
		ui.name_combo->addItem(add_avatar);
		ui.name_combo->setCurrentIndex(0);
	}
	else
	{
		QStringList groups = settings.childGroups();
		for(int i = 0;i < groups.size();++i)
			ui.name_combo->addItem(groups.at(i));
		ui.name_combo->addItem(add_avatar);
		for(int i = 0;i < ui.name_combo->count();++i)
		{
			if(current_avatar == ui.name_combo->itemText(i))
			{
				ui.name_combo->setCurrentIndex(i);
				break;
			}
		}
		LoadDetails(current_avatar);
	}
	settings.endGroup();	// Avatars

	connect(ui.connect_btn,SIGNAL(clicked()),this,SLOT(Connect()));
	connect(ui.cancel_btn,SIGNAL(clicked()),this,SLOT(Cancel()));
	connect(ui.name_combo,SIGNAL(currentTextChanged(const QString&)),this,SLOT(AvatarChanged(const QString&)));
	connect(ui.delete_rec_btn,SIGNAL(clicked()),this,SLOT(DeleteRec()));
}

LoginDialog::~LoginDialog()
{

}


void	LoginDialog::AvatarChanged(const QString& new_name)
{
	if(new_name != add_avatar)
		LoadDetails(new_name);
	else
	{
		ui.avatar_edit->clear();
		ui.account_edit->clear();
		ui.pwd_edit->clear();
		ui.save_pwd_check->setChecked(false);
	}
}

void	LoginDialog::Cancel()	
{ 
	hide(); 
}

void		LoginDialog::Connect()
{
	if((ui.address_edit->text() == "") || (ui.port_edit->text() == ""))
	{
		FedTerm::MainWindow()->Message("Login","You must provide a server address and a port number!");
		return;
	}

	if((ui.account_edit->text() == "") || (ui.account_edit->text() == "") || (ui.avatar_edit->text() == ""))
	{
		FedTerm::MainWindow()->Message("Login","You must provide an avatar name,\nan account ID and a password!");
		return;
	}

	login_rec->host = ui.address_edit->text();
	login_rec->port = ui.port_edit->text().toUInt();
	login_rec->avatar_name = ui.avatar_edit->text();
	login_rec->account_id = ui.account_edit->text();
	login_rec->password = ui.pwd_edit->text();
	login_rec->save_pwd = ui.save_pwd_check->isChecked();
	emit LoginInfoReady();
}

bool LoginDialog::eventFilter(QObject *target, QEvent *event)
{
	if((target == ui.image_widget) && (event->type() == QEvent::Paint))
	{
		QPainter	painter(ui.image_widget);
		painter.drawImage(0, 0, picture);
		return(true);
	}
	return(QWidget::eventFilter(target, event));
}

void		LoginDialog::LoadDetails(const QString& current_avatar)
{
	QSettings	settings(QSettings::IniFormat,QSettings::UserScope,"ibgames", "FedTerm");
	settings.beginGroup("Avatars");
	settings.beginGroup(current_avatar);
	ui.address_edit->setText(settings.value("host_name","fed2.ibgames.net").toString());
	ui.port_edit->setText(QString::number(settings.value("port",30003).toUInt()));
	ui.avatar_edit->setText(settings.value("name","").toString());
	ui.account_edit->setText(settings.value("account_id","").toString());
	ui.pwd_edit->setText(settings.value("password","").toString());
	ui.save_pwd_check->setChecked(ui.pwd_edit->text() != "");
	settings.endGroup();	// Avatar name (current avatar)
	settings.endGroup();	// Avatars
}



void	LoginDialog::DeleteRec()
{
	if(ui.name_combo->currentIndex() == ui.name_combo->count() -1)
		return;
	QSettings	settings(QSettings::IniFormat,QSettings::UserScope,"ibgames", "FedTerm");
	settings.beginGroup("Avatars");
	settings.remove(ui.avatar_edit->text());
	settings.endGroup();

	ui.name_combo->removeItem(ui.name_combo->currentIndex());
	ui.name_combo->setCurrentIndex(ui.name_combo->count() -1);
}
