/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#ifndef WEAPONS_H
#define WEAPONS_H

#include <list>
#include <string>
#include <utility>

using AttribList = std::list<std::pair<std::string,std::string>>;
using WeaponInfo = std::pair<std::string,std::string>; 

class StatTable;

class Weapons
{
public:
	static const int	WRITE_LATER = -1;
	static const int	CURRENT_START = -2;

	enum { MISSILES, RACK, LASER, TL, QL, MAX_WEAPON_STATS };

private:
	static const int	UNKNOWN_VAL = -1;
	static const int	MAX_WEAPON_SLOTS = 4;

	static const std::string	xml_stat_names[];
	static const std::string	display_names[];
	static const WeaponInfo		empty_slot;

	std::string	missiles;
	WeaponInfo	weapon_slots[MAX_WEAPON_SLOTS];

	int	current_start;
	int	slot_index;

	void	MakeWeaponAndPercentage(const AttribList& attribs,int name_index);

public:
	Weapons();
	~Weapons()	{	}

	void	Clear();
	int	EndUpdate(int start_index);
	void	ProcessInfo(const AttribList& attribs);
	void	StartUpdate();
};
#endif

