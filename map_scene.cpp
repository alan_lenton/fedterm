/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#include "map_scene.h"

#include <QtGlobal>
#include <QList>

#include "fed_map.h"
#include "fedterm.h"
#include "tiles.h"

MapScene::MapScene(FedMap *f_map,QObject *parent) : QGraphicsScene(parent)
{
	height = FedMap::HEIGHT * 2 - 1;
	width = FedMap::WIDTH * 2 - 1;

	setSceneRect(0,0,width * Tiles::WIDTH,height * Tiles::HEIGHT);
	background_color = QColor::fromRgb(180,180,180);
	setBackgroundBrush(background_color);

	fed_map = f_map;
	tiles = new Tiles;

	InitialiseMap();
}

MapScene::~MapScene()
{
	delete tiles;
}


void	MapScene::AddCurrentMarker(int abs_row,int abs_column,QGraphicsItem *item)
{
	if(item == nullptr)
		item =  addPixmap(*tiles->Tile(Tiles::CURRENT));
	item->setZValue(1);
	item->setPos(abs_row * Tiles::HEIGHT + 2,abs_column * Tiles::WIDTH + 2);
	FedTerm::MainWindow()->CentreMiniMap(abs_row * Tiles::HEIGHT,abs_column * Tiles::WIDTH);
}

void	MapScene::AddTile(int abs_row,int abs_column,int tile_num)
{
	QGraphicsItem	*item =  addPixmap(*tiles->Tile(tile_num));
	item->setPos(abs_row * Tiles::HEIGHT,abs_column * Tiles::WIDTH);
	FedTerm::MainWindow()->CentreMiniMap(abs_row * Tiles::HEIGHT,abs_column * Tiles::WIDTH);
}

void	MapScene::ChangeTile(int abs_row,int abs_column,int tile_num,const std::string& name)
{
	QGraphicsItem *item = itemAt(QPointF(qreal(abs_row *Tiles::HEIGHT),qreal(abs_column * Tiles:: WIDTH)),QTransform());
	if(item != nullptr)
		removeItem(item);
	delete item;

	item = addPixmap(*tiles->Tile(tile_num));
	item->setPos(abs_row * Tiles::HEIGHT,abs_column * Tiles::WIDTH);
	item->setToolTip(QString::fromStdString(name));
}

QColor	MapScene::GetBgColor()
{
	return backgroundBrush().color();
}

void	MapScene::InitialiseMap()
{
	clear();
	setBackgroundBrush(QColor(background_color));
}

QGraphicsItem	*MapScene::RemoveCurrentMarker(int abs_row,int abs_column)
{
	// NOTE: It should be possible to reuse the marker (item) but for some reason 
	// that doesn't seem to work. Deleting it here and recreating it in
	// AddCurrentMarker() does work. This needs investigating further - when I have the time!
	QGraphicsItem *item = itemAt(QPointF(qreal(abs_row *Tiles::HEIGHT +2),qreal(abs_column * Tiles:: WIDTH+2)),QTransform());
	if(item != nullptr)
		removeItem(item);	
	delete item;
	item = nullptr;
	return item;
}

void	MapScene::SetBgColor(const QColor& color)
{
	background_color = color;
	setBackgroundBrush(background_color);
}


