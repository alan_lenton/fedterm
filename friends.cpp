/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#include "friends.h"

#include <algorithm>
#include	<sstream>

#include <QList>
#include <QMessageBox>
#include <QSettings>
#include <QStringList>

#include "fedterm.h"
#include "soundfx.h"

const std::string	Friends::rank_names[] = 
{
	"Groundhog", "Commander", "Captain", "Adventurer", "Merchant", "Trader", 
	"Industrialist", "Manufacturer", "Financier", "Founder", "Engineer", 
	"Mogul", "Technocrat", "Gengineer","Magnate", "Plutocrat", 
};

Friends::Friends(QWidget *parent) : QWidget(parent)
{
	ui.setupUi(this);
	SetUpFriends();
	SetUpRanks();

	connect(ui.add_name_btn,SIGNAL(clicked()),this,SLOT(AddName()));
	connect(ui.audio_check,SIGNAL(clicked()),this,SLOT(AudioCheck()));
	connect(ui.cancel_btn,SIGNAL(clicked()),this,SLOT(ScrapChanges()));
	connect(ui.clear_friends_btn,SIGNAL(clicked()),this,SLOT(ClearAllFriends()));
	connect(ui.clear_ranks_btn,SIGNAL(clicked()),this,SLOT(ClearAllRanks()));
	connect(ui.delete_name_btn,SIGNAL(clicked()),this,SLOT(DeleteName()));
	connect(ui.friends_list,SIGNAL(clicked(const QModelIndex&)),this,SLOT(ListClicked(const QModelIndex&)));
	connect (ui.ok_btn,SIGNAL(clicked()),this,SLOT(SaveAndExit()));
}

Friends::~Friends()
{

}


void	Friends::AddName()
{
	if(ui.new_name_edit->text().isEmpty())
	{
		QMessageBox::warning(this,"Add A Friend","Please enter your friend's\nname into the box provided.");
		ui.new_name_edit->setFocus();
		return;
	}

	QString	lc_name(ui.new_name_edit->text().toLower());
	QString	name(lc_name.replace(0,1,lc_name[0].toUpper()));
	for(auto a_friend: friends)
	{
		if(a_friend == name.toStdString())
		{
			QMessageBox::warning(this,"Add A Friend","This person is already in your friends list!");
			ui.new_name_edit->setText("");
			ui.new_name_edit->setFocus();
			return;
		}
	}

	ui.friends_list->addItem(name);
	ui.new_name_edit->setText("");
	ui.new_name_edit->setFocus();
}

void Friends::AudioCheck()	{ audio_wanted = ui.audio_check->isChecked(); }

void	Friends::ClearAllFriends()
{
	if(ui.friends_list->count() == 0)
	{
		QMessageBox::information(this,"Clear Friends List","You are already friendless!\nA sad state of affairs...");
		return;
	}

	if(QMessageBox::question(this,"Clear Friends List","Are you sure? You will have to re-enter\nthem if you change your mind!") == QMessageBox::Yes)
		ui.friends_list->clear();
}

void	Friends::ClearAllRanks()
{
	for(auto rank: rank_checks)
		rank->setChecked(false);
}

void	Friends::DeleteName()
{
	if(ui.new_name_edit->text().isEmpty())
	{
		QMessageBox::warning(this,"Remove A Friend","Please select the name you wish\nto delete into the box provided.");
		ui.new_name_edit->setFocus();
		return;
	}

	QString	lc_name(ui.new_name_edit->text().toLower());
	QString	name(lc_name.replace(0,1,lc_name[0].toUpper()));
	QList<QListWidgetItem *> items = ui.friends_list->findItems(name,Qt::MatchFixedString);
	if(items.empty())
	{
		QMessageBox::warning(this,"Remove A Friend","Unable to find that name\nin your friends list!");
		ui.new_name_edit->setText("");
		ui.new_name_edit->setFocus();
		return;
	}
	delete items[0];

	ui.new_name_edit->setText("");
	ui.new_name_edit->setFocus();
}

void	Friends::ListClicked(const QModelIndex& which)
{
	ui.new_name_edit->setText(ui.friends_list->currentItem()->text());
}

void	Friends::Notify(std::string name,int rank,int notify_type)
{
	bool	do_report = false;
	if((rank >= 0) && ranks_to_report[rank])
		do_report = true;
	if((!friends.empty() && (std::find(friends.cbegin(),friends.cend(),name) != friends.cend())))
		do_report = true;

	if(do_report)
	{
		std::ostringstream	buffer;
		if(rank >= 0)
			buffer << rank_names[rank] << " ";
		buffer << name << " has ";
		buffer << ((notify_type == LOGIN) ? "logged in" : "logged out") << ".\n";
		FedTerm::MainWindow()->DisplayMain(buffer.str());
		if(audio_wanted)
			FedTerm::MainWindow()->PlaySound(SoundFx::FRIENDS);
	}
}

void	Friends::SaveAndExit()
{
	friends.clear();
	int num_items = ui.friends_list->count();
	for(int count = 0;count < num_items;++count)
		friends.push_back(ui.friends_list->item(count)->text().toStdString());
	SaveFriends();

	for(int count = 0;count < MAX_RANKS;++count)
		ranks_to_report[count] = rank_checks[count]->isChecked();
	audio_wanted = ui.audio_check->isChecked();
	SaveRanks();
	
	hide();
}

void	Friends::SaveFriends()
{
	QSettings	settings(QSettings::IniFormat,QSettings::UserScope,"ibgames","FedTerm");

	settings.beginGroup("FriendAlerts");
	settings.remove("");
	if(!friends.empty())
	{
		for(auto a_friend: friends)
			settings.setValue(QString::fromStdString(a_friend),true);
	}
	settings.endGroup();
}

void	Friends::SaveRanks()
{
	QSettings	settings(QSettings::IniFormat,QSettings::UserScope,"ibgames","FedTerm");

	settings.beginGroup("RankAlerts");
	for(int count = 0;count < MAX_RANKS;++count)
		settings.setValue(QString::fromStdString(rank_names[count]),ranks_to_report[count]);
	settings.setValue("AudioWanted",audio_wanted);
	settings.endGroup();
}

void	Friends::ScrapChanges()
{
	ui.friends_list->clear();
	if(!friends.empty())
	{
		for(auto a_friend: friends)
			ui.friends_list->addItem(QString::fromStdString(a_friend));
	}

	for(int count = 0;count < MAX_RANKS;++count)
		rank_checks[count]->setChecked(ranks_to_report[count]);
	ui.audio_check->setChecked(audio_wanted);

	QMessageBox::information(this,"Scrap Changes","Friends settings have been restored.");
	hide();
}

void	Friends::SetUpFriends()
{
	friends.clear();
	QSettings	settings(QSettings::IniFormat,QSettings::UserScope,"ibgames", "FedTerm");

	settings.beginGroup("FriendAlerts");
	QStringList	keys = settings.allKeys();
	if(!keys.isEmpty())
	{
		 for(QStringList::const_iterator iter = keys.constBegin();iter != keys.constEnd();++iter)
		 {
			 friends.push_back((*iter).toStdString());
			 ui.friends_list->addItem(*iter);
		 }
	}
	settings.endGroup();
}

void	Friends::SetUpRanks()
{
	rank_checks[0] = ui.groundhog_check;
	rank_checks[1] = ui.commander_check;
	rank_checks[2] = ui.captain_check;
	rank_checks[3] = ui.adventurer_check;
	rank_checks[4] = ui.merchant_check;
	rank_checks[5] = ui.trader_check;
	rank_checks[6] = ui.industrialist_check;
	rank_checks[7] = ui.manufacturer_check;
	rank_checks[8] = ui.financier_check;
	rank_checks[9] = ui.founder_check;
	rank_checks[10] = ui.engineer_check;
	rank_checks[11] = ui.mogul_check;
	rank_checks[12] = ui.technocrat_check;
	rank_checks[13] = ui.magnate_check;
	rank_checks[14] = ui.plutocrat_check;
	rank_checks[15] = ui.syndicrat_check;

	QSettings	settings(QSettings::IniFormat,QSettings::UserScope,"ibgames", "FedTerm");

	settings.beginGroup("RankAlerts");
	for(int count = 0;count < MAX_RANKS;++count)
	{
		ranks_to_report[count] = settings.value(QString::fromStdString(rank_names[count]),false).toBool();
		rank_checks[count]->setChecked(ranks_to_report[count]);
	}
	audio_wanted = settings.value("AudioWanted",false).toBool();
	ui.audio_check->setChecked(audio_wanted);
	settings.endGroup();
}

