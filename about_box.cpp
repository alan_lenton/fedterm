/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#include "about_box.h"

#include <QPainter>

#include <fedterm.h>

AboutBox::AboutBox(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);

	pic_image = QImage(":/FedTerm/Images/about_box.png");
	ui.pic_widget->installEventFilter(this);
	QString	version_no("FedTerm ");
	version_no += QString::fromStdString(FedTerm::development_tag);
	ui.label->setText(version_no);
}

AboutBox::~AboutBox()
{

}


bool AboutBox::eventFilter(QObject *target, QEvent *event)
{
	if(event->type() == QEvent::Paint)
	{
		if(target == ui.pic_widget)
		{
			QPainter	painter(ui.pic_widget);
			painter.drawImage(0,0,pic_image);
			return true;
		}
	}

	return(QWidget::eventFilter(target, event));
}