/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#ifndef LOGGING_H
#define LOGGING_H

#include <fstream>
#include <string>

class Logging
{
private:
	std::ofstream	log_file;
	std::ofstream	comms_file;

	bool	logging;
	bool	comms_logging;

	void	MakeBaseFileName(std::string& base_name);

public:
	Logging();
	~Logging();

	bool	DumpScreenBuffer(const std::string& text);
	bool	StartCommsLog();
	bool	StartLog();

	void	CommsLog(const std::string& text);
	void	Log(const std::string& text);
	void	StopLogging();
};

#endif