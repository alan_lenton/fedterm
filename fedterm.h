/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#ifndef FEDTERM_H
#define FEDTERM_H

#include <memory>
#include <string>

#include <QImage>
#include <QtWidgets/QMainWindow>
#include <QTcpSocket>
#include <QTimer>

#include "ui_fedterm.h"

class AboutBox;
class Comms;
class FedMap;
class LoginDialog;
class Logging;
class LoginRec;
class MapScene;
class NewAvatarName;
class Newbie;
class NewbieRec;
class SoundFx;
class StatTable;
class UserMacros;

using AttribList = std::list<std::pair<std::string,std::string>>;

class FedTerm : public QMainWindow
{
	Q_OBJECT
public:
	static const std::string	version;
	static const std::string	comms_level;
	static const std::string	development_tag;
	static const std::string	base_dir;

private:
	static FedTerm		*main_window;

	static const int	MAX_AVATAR_INF_ROWS = 30;
	static const int	MAX_INPUT_CHARS = 1024;
	static const int	STATUS_BOX_MIN_WIDTH = 220;
	static const int	STATUS_TABLE_ROW_HEIGHT = 20;
	static const int	STATUS_TABLE_MIN_COL0_WIDTH = 100;
	static const int	STATUS_TABLE_MIN_COL1_WIDTH = 76;
	static const int	STATUS_TABLE_MIN_WIDTH = 200;

	Ui::FedTerm ui;

	AboutBox		*about;
	Comms			*comms;
	FedMap		*fed_map;
	Newbie		*newbie_dialog;
	LoginDialog	*login_dialog;
	LoginRec		*login_rec;
	MapScene		*scene;
	UserMacros	*user_macros;
	Logging		*logger;
	SoundFx		*sound_fx;
	bool			logging;
	NewAvatarName	*new_name_dialog;

	QImage	avatar_image;
	QImage	players_image;
	QImage	default_image;
	QImage	ship_image;

	QImage	planet_image;
	QImage	hyper_link_image;

	QImage	exchange_image;
	QImage	exchange_default;

	QString	ticker_text;
	int		ticker_index;
	QTimer	*ticker_timer;

	bool	eventFilter(QObject *target, QEvent *event);
	
	void	SetStatusTableWidths(int frame_width);
	void	ClearDisplay();
	void	SendMvtCmd(int direction);
	void	SetUpExchangeTable();
	void	SetUpMenuConnections();
	void	SetUpMovementConnections();
	void	UpdateFont(const QFont font);

protected:
	void	closeEvent(QCloseEvent *event) override;
	void	resizeEvent(QResizeEvent *event) override;

private slots:
	void	About();
	void	AutoExchangeSwitch();
	void	BackgroundColour();
	void	BuyExtras();
	void	ChangeFont();
	void	Connect();
	void	ExchangeCopy();
	void	ExchangeTableDClicked(int row,int col);
	void	ExitConf();
	void	ExitProgram();
	void	Feedback();
	void	FtManual();
	void	GameManual();
	void	HideMovementArrows();
	void	LogAll();
	void	LoginInfoReady();	
	void	MainTabChanged(int index);
	void	MapsHelp();
	void	Move(bool);
	void	NewbieConnect();
	void	NewbieInfoReady();
	void	PlanetListClicked(QListWidgetItem *item);
	void	PlayerListClicked(QListWidgetItem *item);
	void	QuickStartGuide();
	void	SaveScreenBuffer();
	void	ShowFkeys();
	void	ShowFriends();
	void	Splitter2Moved(int,int);
	void	StartLogging();
	void	StatsPanel(bool checked);
	void	StopLogging();
	void	SystemListClicked(QListWidgetItem *item);
	void	TextAlert();
	void	TMCVoting();
	void	UpdateCommodityTicker();

public slots:
	void	LogOff();

public:
	static FedTerm	*MainWindow();
	static bool	MakeDirectory(const std::string& path);

	FedTerm(QWidget *parent = 0);
	~FedTerm();

	QTableWidget *GetStatusTable()	{ return ui.avatar_table; }
	QString	StartMacro();
	FedMap	*GetMap()					{ return fed_map; }
	int	FindStatRow(const std::string& stat_name);
	bool	WantsExchCopy()	{ return(ui.action_copy_all_exch->isChecked());	}
	bool	WantsTextAlert()	{ return(ui.action_text_alert->isChecked()); }

	void	AddExchangeTitle(const std::string& name);
	void	AddPlanet(const std::string& name);
	void	AddPlanetBuild(const std::string& name);
	void	AddPlanetInfo(const std::string& info);
	void	AddPlanetTitle(const std::string& info);
	void	AddPlayer(const std::string& name);
	void	AddSystem(const std::string& name);
	void	AddWdfInfo(const std::string& name);
	void	CentreMiniMap(int x,int y);
	void	ClearExchangeDisplay();
	void	ClearPlanetDisplays(bool inc_planet_list = false);
	void	ClearStatsPanel()				{ ui.avatar_table->clear(); }
	void	ConnectionClosed();
	void	DisplayCargoLine(const std::string& text);
	void	DisplayMain(const char *text);
	void	DisplayMain(const std::string& text);
	void	DisplayManifest(const std::string& text);
	void	HideLoginDialog();
	void	HideNewbieDialog();
	void	Message(const QString& title,const QString& text);
	void	PlaySound(int sound);
	void	ProvideNewAvatarName();
	void	RemovePlayer(const std::string& name);
	void	SaveLoginRec();
	void	SaveLoginRec(const NewbieRec& rec);
	void	SendCmd(const QString command);
	void	SetAvatarName(const std::string& name);
	void	SetConnectDisplay();
	void	SetInfoBar(const std::string& new_info);
	void	SetInfraScrollBar();
	void	SetInputFocus();
	void	SetLoggingBar(bool new_status);
	void	SetStat(const std::string& name,const std::string& value,int row);
	void	SetStatusBar(const std::string& new_status);
	void	SetLocInfo(const std::string& loc_info);
	void	SetTitle(const std::string& title);
	void	StartCommodityTicker(const std::string& text);
	void	Switch2Exchange();
	void	UpdateAvatarPic(const std::string& name);
	void	UpdateCommodityName(const std::string& name);
	void	UpdateCommodityPic(const std::string& name);
	void	UpdateCommodityPrice(const std::string& price);
	void	UpdateCommodityQuantity(const std::string& quantity);
	void	UpdateExchangeLine(int row,const std::string& buy_sell,const std::string& stock,const std::string& price);
	void	UpdatePlanetPic(const std::string& name);
	void	UpdatePlayerNumBar();
	void	UpdatePlayersPic(const std::string& name);
	void	UpdateShipPic(const std::string& name);
};

#endif // FEDTERM_H
