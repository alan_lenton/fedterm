/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#ifndef LOCATION_H
#define LOCATION_H

#include <array>
#include <bitset>
#include <fstream>
#include <list>
#include <memory>
#include <string>
#include <utility>

#include <xml_parser.h>

using ExitList = std::unique_ptr<std::list<std::pair<int,int>>>;

class Location
{
public:
	enum { NORTH, NE, EAST, SE, SOUTH, SW, WEST, NW, UP, DOWN, MAX_EXITS };
	enum
	{
		SPACE, LINK, EXCHANGE, SHIPYARD, REPAIR, HOSPITAL, INSURE, PEACE,
		BAR, COURIER, UNLIT, CUSTOM, TELEPORT, PICKUP, MAX_FLAGS
	};

	static const int	INVALID_LOC = -1;
	static const int	NO_EXIT = -1;
	static const std::string	exit_names[];

private:
	int	loc_no;
	std::string	name;
	std::array<int,MAX_EXITS>	exits;
	std::bitset<MAX_FLAGS>		flags;

public:
	Location(int loc_num,std::string& the_name);
	Location(const AttribList& attribs);
	~Location()	{	}

	ExitList GetExits();
	const std::string	Name()			{ return name; }	 
	int	Number()							{ return loc_no; }

	bool	IsLink()							{ return(flags.test(LINK)); }
	bool	IsPeace()						{ return(flags.test(PEACE)); }

	void	SetExit(int dir,int exit)	{ exits[dir] = exit; }
	void	SetFlag(int which)			{ flags.set(which);	}
	void	Write(std::ofstream& file);
};

#endif