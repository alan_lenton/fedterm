/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#include "map_parser.h"

#include <stdexcept>

#include <QXmlInputSource>
#include <QXmlSimpleReader>

#include "fed_map.h"
#include "location.h"
#include "xml_parser.h"


MapParser::MapParser(const std::string f_name, FedMap *the_map)
{
	fed_map = the_map;
	location = nullptr;
	file_name = QString::fromStdString(f_name);
}

MapParser::~MapParser()
{
	if(file != nullptr)
	{
		file->close();
		delete file;
	}
}


bool	MapParser::endElement(const QString& namespaceURI,const QString& localName,const QString& name)
{
	if(name == "fed-loc")
	{
		fed_map->AddLoc(location);
		location = nullptr;
	}
	return true;
}

bool MapParser::Parse()
{
	file = new QFile(file_name);
	if(!file->open(QIODevice::ReadOnly | QIODevice::Text))
	{
		file = nullptr;
		return false;
	}

	QXmlInputSource	source(file);
	QXmlSimpleReader	reader;
	reader.setContentHandler(this);
	return reader.parse(source);
	return true;
}

bool	MapParser::startElement(const QString& namespaceURI,const QString& localName,
										const QString& qName, const QXmlAttributes& atts)
{
	if(qName == "fed-loc")
		StartLocation(atts);
	return true;
}

void	MapParser::StartLocation(const QXmlAttributes& attribs)
{

	int	loc_no = attribs.value(QString::fromLatin1("loc-num")).toInt();
	std::string the_name= attribs.value(QString::fromLatin1("name")).toStdString();
	location = new Location(loc_no,the_name);

	QString	where_to;
	for(int count = 0;count < Location::MAX_EXITS;++count)
	{
		where_to = attribs.value(QString::fromStdString(Location::exit_names[count]));
		if(where_to == "")
			location->SetExit(count,Location::NO_EXIT);
		else
			location->SetExit(count,where_to.toInt());
	}

	std::string	flags = attribs.value(QString::fromLatin1("flags")).toStdString();
	if(flags != "")
	{
		if(flags.find('s') != std::string::npos)
			location->SetFlag(Location::SPACE);
		if(flags.find('p') != std::string::npos)
			location->SetFlag(Location::PEACE);
		if(flags.find('l') != std::string::npos)
			location->SetFlag(Location::LINK);
	}
}

