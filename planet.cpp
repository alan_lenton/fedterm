/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#include "planet.h"

#include "fedterm.h"
#include "server_parser.h"

Planet::Planet(const AttribList& attribs,ServerParser *parser)
{
	infra_state = NONE;

	parser->FindAttrib(attribs,"name",name);
	std::string bodge;
	parser->FindAttrib(attribs,"system",bodge);
	auto comma_index = bodge.find(',');
	auto apos_index = bodge.find('\'');

	if((comma_index != std::string::npos) && (apos_index != std::string::npos))
	{
		cartel = bodge.substr(0,comma_index - 6);
		star = bodge.substr(comma_index + 2,apos_index - (comma_index + 2));
	}
	else
	{
		cartel = "Unknown";
		star = "Unknown";
	}
	FedTerm::MainWindow()->AddPlanetTitle(name + " in the " + star + " system");
	FedTerm::MainWindow()->AddPlanetInfo("Planet: " + name);
	FedTerm::MainWindow()->AddPlanetInfo("Star: " + star);
	FedTerm::MainWindow()->AddPlanetInfo("Cartel: " + cartel);

	std::string	owner;
	parser->FindAttrib(attribs,"owner",owner);
	FedTerm::MainWindow()->AddPlanetInfo("Owner: " + owner);

	std::string treasury;
	parser->FindAttrib(attribs,"treasury",treasury);
	for(int index = treasury.length() - 3;index > 0;index -= 3)
		treasury.insert(index,",");
	FedTerm::MainWindow()->AddPlanetInfo("Treasury: " + treasury);

	std::string economy;
	parser->FindAttrib(attribs,"economy",economy);
	FedTerm::MainWindow()->AddPlanetInfo("Economy: " + economy);

	std::string total_wf, avail_wf;
	parser->FindAttrib(attribs,"total-wf",total_wf);
	parser->FindAttrib(attribs,"avail-wf",avail_wf);
	FedTerm::MainWindow()->AddPlanetInfo("Workforce: " + avail_wf + "/" + total_wf);

	std::string	yard;
	parser->FindAttrib(attribs,"yard",yard);
	FedTerm::MainWindow()->AddPlanetInfo("Shipyard Markup: " + yard + "%");

	std::string	approval;
	parser->FindAttrib(attribs,"disaffection",approval);
	if(approval[0] == '-')
		FedTerm::MainWindow()->AddPlanetInfo("Approval Rating: " + approval.substr(1) + "%");
	else
		FedTerm::MainWindow()->AddPlanetInfo("Disaffection: " + approval.substr(0) + "%");

	std::string	fleet;
	parser->FindAttrib(attribs,"fleet",fleet);
	if(fleet != "")
		FedTerm::MainWindow()->AddPlanetInfo("Merchant Fleet: " + fleet + " ships");

	FedTerm::MainWindow()->UpdatePlanetPic(name);
}

void	Planet::Depots(const AttribList& attribs,ServerParser *parser)
{
	if(infra_state != DEPOTS)
	{
		FedTerm::MainWindow()->AddWdfInfo(" ");
		FedTerm::MainWindow()->AddWdfInfo("  - Depots -");
		infra_state = DEPOTS;
	}

	std::string	depot;
	parser->FindAttrib(attribs,"name",depot);
	FedTerm::MainWindow()->AddWdfInfo(depot);
}

void	Planet::Factories(const AttribList& attribs,ServerParser *parser)
{
	if(infra_state != FACTORIES)
	{
		FedTerm::MainWindow()->AddWdfInfo(" ");
		FedTerm::MainWindow()->AddWdfInfo("  - Factories -");
		infra_state = FACTORIES;
	}

	std::string	factory;
	parser->FindAttrib(attribs,"info",factory);
	FedTerm::MainWindow()->AddWdfInfo(factory);
}

void	Planet::InfraBuilds(const AttribList& attribs,ServerParser *parser)
{
	std::string	build;
	parser->FindAttrib(attribs,"info",build);
	FedTerm::MainWindow()->AddPlanetBuild(build);
	if(build.find("Total Infrastructure Builds:") !=  std::string::npos)
		FedTerm::MainWindow()->SetInfraScrollBar();
}

void	Planet::Warehouses(const AttribList& attribs,ServerParser *parser)
{
	if(infra_state != WAREHOUSES)
	{
		FedTerm::MainWindow()->AddWdfInfo(" ");
		FedTerm::MainWindow()->AddWdfInfo("  - Warehouses -");
		infra_state = WAREHOUSES;
	}

	std::string	warehouse;
	parser->FindAttrib(attribs,"name",warehouse);
	FedTerm::MainWindow()->AddWdfInfo(warehouse);
}



