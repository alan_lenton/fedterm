/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#include "soundfx.h"

#include <QDir>
#include <QUrl>

#include "fedterm.h"

SoundFx::SoundFx(QObject *parent) : QObject(parent)
{
	sound_dir = QString::fromStdString(FedTerm::base_dir) + "/sounds/";
}

void	SoundFx::Play(int sound)
{
	switch(sound)
	{
		case ALERT:		QSound::play(QDir::toNativeSeparators(sound_dir + "alert.wav"));		break;
		case FRIENDS:	QSound::play(QDir::toNativeSeparators(sound_dir + "friends.wav"));	break;
	}
}

