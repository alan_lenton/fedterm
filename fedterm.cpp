/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#include "fedterm.h"

#include <fstream>
#include <sstream>

#include <QColorDialog>
#include <QDesktopServices>
#include <QDir>
#include <QFile>
#include <QFileDialog>
#include <QFontDialog>
#include <QMessageBox>
#include <QPainter>
#include <QPalette>
#include <QScrollBar>
#include <QSettings>
#include <QStringList>

#include "about_box.h"
#include "avatar.h"
#include "comms.h"
#include "exchange.h"
#include "fed_map.h"
#include "location.h"
#include "logging.h"
#include "login_dialog.h"
#include "map_scene.h"
#include "new_avatar_name.h"
#include "newbie.h"
#include "soundfx.h"
#include "stats.h"
#include "stat_table.h"
#include "tiles.h"
#include "user_macros.h"

// Be very careful if you mix C++11/14 stuff with Qt libraries - it could well end in tears

FedTerm	*FedTerm::main_window = 0;
const std::string	FedTerm::version("3.01");
const std::string FedTerm::comms_level("4");
const std::string	FedTerm::development_tag("3.01.02");

const std::string FedTerm::base_dir(QDir::currentPath().toStdString());

FedTerm::FedTerm(QWidget *parent) : QMainWindow(parent)
{
	ui.setupUi(this);

	QSettings	settings(QSettings::IniFormat,QSettings::UserScope,"ibgames", "FedTerm");

	resize(settings.value("size",QSize(1024,768)).toSize());
	move(settings.value("pos",QPoint(50,50)).toPoint());

	QVariant		variant = settings.value("Font");
	if(variant.isValid())
		UpdateFont(variant.value<QFont>());
	
	bool stats_panel = settings.value("Stats Panel",true).toBool();
	ui.action_stats->setChecked(stats_panel);
	if(stats_panel)
		ui.left_frame->hide();
	else
		ui.left_frame->show();
	
	ui.action_auto_exch->setChecked(settings.value("AutoExchange",false).toBool());
	ui.action_copy_all_exch->setChecked(settings.value("exch_copy",false).toBool());
	ui.action_exit_conf->setChecked(settings.value("exit_conf",false).toBool());

	QString win_title = "FedTerm version " + QString::fromStdString(version);
	setWindowTitle(win_title);

	statusBar()->setFont(this->font());
	statusBar()->addWidget(ui.spacer_label);
	statusBar()->addWidget(ui.status_label);
	statusBar()->addWidget(ui.logging_label);
	statusBar()->addWidget(ui.players_label);
	statusBar()->addWidget(ui.loc_label);
	statusBar()->addPermanentWidget(ui.info_label);
	ui.status_label->setText("Not connected");

	ui.output_text_edit->document()->setMaximumBlockCount(250);

	about = new AboutBox();
	comms = new Comms(this);
	login_rec = new LoginRec;
	newbie_dialog = new Newbie(this);
	login_dialog = new LoginDialog(login_rec,this);
	user_macros = new UserMacros();
	user_macros->hide();
	logger = new Logging;
	logging = false;
	sound_fx =  new SoundFx(this);
	new_name_dialog = new NewAvatarName(this);

	ui.action_text_alert->setChecked(settings.value("Incoming Text Alert",false).toBool());
	comms->WantTextAlerts(ui.action_text_alert->isChecked());
	

	if(settings.value("auto_logging",false).toBool())
	{
		ui.action_log_all->setChecked(true);
		StartLogging();
	}

	QColor bg_colour = settings.value("bg_colour").value<QColor>();
	if(bg_colour.isValid())
	{
		QPalette	new_palette(palette());
		new_palette.setColor(QPalette::Base,bg_colour);
		new_palette.setColor(QPalette::Window,bg_colour);
		setAutoFillBackground(true);
		setPalette(new_palette);
		
		ui.left_frame->setAutoFillBackground(true);
		ui.left_frame->setPalette(new_palette);
		ui.right_frame->setAutoFillBackground(true);
		ui.right_frame->setPalette(new_palette);
		ui.frame_5->setAutoFillBackground(true);
		ui.frame_5->setPalette(new_palette);
		ui.frame_7->setAutoFillBackground(true);
		ui.frame_7->setPalette(new_palette);
		ui.frame_14->setAutoFillBackground(true);
		ui.frame_14->setPalette(new_palette);
		ui.frame_15->setAutoFillBackground(true);
		ui.frame_15->setPalette(new_palette);
		ui.exch_table->setAutoFillBackground(true);
		ui.exch_table->setPalette(new_palette);

	}
	QString q_base_dir(QString::fromStdString(base_dir) + "/");
	default_image = QImage(q_base_dir + "pictures/interference.png");
	avatar_image = default_image;
	players_image = default_image;

	ship_image = QImage(q_base_dir + "pictures/fed2.png");

	hyper_link_image = QImage(q_base_dir + "planets/hyperspace_link.png");
	planet_image = hyper_link_image;

	exchange_default = QImage(q_base_dir + "exchanges/exchange.png");
	exchange_image = exchange_default;

	// Avatar etc stats display set up
	for(auto count = 0;count < MAX_AVATAR_INF_ROWS;++count)
		ui.avatar_table->setRowHeight(count,STATUS_TABLE_ROW_HEIGHT);
	SetStatusTableWidths(ui.left_frame->frameRect().width());


	ui.input_text_edit->installEventFilter(this);
	ui.input_text_edit2->installEventFilter(this);
	ui.avatar_pic->installEventFilter(this);
	ui.players_pic->installEventFilter(this);
	ui.ship_pic->installEventFilter(this);
	ui.ship_pic_2->installEventFilter(this);
	ui.planet_pic->installEventFilter(this);
	ui.exchange_pic->installEventFilter(this);
	ui.main_tab->installEventFilter(this);

	fed_map = new FedMap();
	scene = new MapScene(fed_map);
	ui.map_view->setScene(scene);
	ui.map_view->ensureVisible(0,0,Tiles::WIDTH,Tiles::HEIGHT);
	fed_map->SetScene(scene);
	ui.mini_map_view->setScene(scene);

	ticker_timer = new QTimer(this);
	SetUpExchangeTable();

	SetUpMenuConnections();
	SetUpMovementConnections();

	// Star System Tab Stuff
	connect(ui.main_tab,SIGNAL(currentChanged(int)),this,SLOT(MainTabChanged(int)));
	connect(ui.planet_list,SIGNAL(itemClicked(QListWidgetItem *)),this,SLOT(PlanetListClicked(QListWidgetItem *)));
	connect(ui.system_list,SIGNAL(itemClicked(QListWidgetItem *)),this,SLOT(SystemListClicked(QListWidgetItem *)));

	// non-menu connections
	connect(login_dialog,SIGNAL(LoginInfoReady()),this,SLOT(LoginInfoReady()));
	connect(ui.player_list,SIGNAL(itemClicked(QListWidgetItem *)),this,SLOT(PlayerListClicked(QListWidgetItem *)));
	connect(ticker_timer,SIGNAL(timeout()),this,SLOT(UpdateCommodityTicker()));
	connect(ui.exch_table,SIGNAL(cellDoubleClicked(int,int)),this,SLOT(ExchangeTableDClicked(int,int)));
	connect(newbie_dialog,SIGNAL(NewbieInfoReady()),this,SLOT(NewbieInfoReady()));
	connect(ui.splitter_2,SIGNAL(splitterMoved(int,int)),this,SLOT(Splitter2Moved(int,int)));

	QVariant	sp_variant = settings.value("splitter");
	if(!sp_variant.isNull())
		ui.splitter->restoreState(sp_variant.toByteArray());

	QVariant	sp2_variant = settings.value("splitter_2");
	if(!sp2_variant.isNull())
		ui.splitter_2->restoreState(sp2_variant.toByteArray());

	main_window = this;
}

FedTerm::~FedTerm()
{
	QSettings	settings(QSettings::IniFormat,QSettings::UserScope,"ibgames", "FedTerm");
	settings.setValue("auto_logging",ui.action_log_all->isChecked());

	// Save main window size and position
	settings.setValue("size",size());
	settings.setValue("pos",pos());
	settings.setValue("splitter",ui.splitter->saveState());
	settings.setValue("splitter_2",ui.splitter_2->saveState());

	delete sound_fx;
	delete user_macros;
	delete login_rec;
	delete ticker_timer;
	delete scene;
	delete fed_map;
	delete comms;
	delete about;
	delete logger; // Leave till last so you can note any close down problems...
}


void	FedTerm::About()	{ about->show(); }
void	FedTerm::AddExchangeTitle(const std::string& name)	{ ui.exch_title_label->setText(QString::fromStdString(name)); }

void	FedTerm::AddPlanet(const std::string& name)
{
	ui.planet_list->addItem(QString::fromStdString(name));
	if(ui.planet_label->text() != "Select a Planet")
		ui.planet_label->setText("Select a Planet");
}

void	FedTerm::AddPlanetBuild(const std::string& info)	{ ui.planet_build_edit->appendPlainText(QString::fromStdString(info)); }
void	FedTerm::AddPlanetInfo(const std::string& info)		{ ui.planet_info_edit->appendPlainText(QString::fromStdString(info)); }
void	FedTerm::AddPlanetTitle(const std::string& info)	{ ui.planet_label->setText(QString::fromStdString(info)); }

void	FedTerm::AddPlayer(const std::string& name)
{
	ui.player_list->addItem(QString::fromStdString(name));
	UpdatePlayerNumBar();
}

void	FedTerm::AddSystem(const std::string& name)			{ ui.system_list->addItem(QString::fromStdString(name)); }
void	FedTerm::AddWdfInfo(const std::string& info)			{ ui.planet_wdf_edit->appendPlainText(QString::fromStdString(info)); }

void	FedTerm::AutoExchangeSwitch()
{
	QSettings	settings(QSettings::IniFormat,QSettings::UserScope,"ibgames", "FedTerm");
	settings.setValue("AutoExchange",ui.action_auto_exch->isChecked());
}

void	FedTerm::BackgroundColour()
{
	QColor colour = palette().color(QPalette::Base);
	QColorDialog *dialog = new QColorDialog(colour,this);
	colour = dialog->getColor(colour);
	if(colour.isValid())
	{
		QPalette	new_palette(palette());
		new_palette.setColor(QPalette::Window,colour);
		new_palette.setColor(QPalette::Base,colour);
		setAutoFillBackground(true);
		setPalette(new_palette);
		ui.left_frame->setAutoFillBackground(true);
		ui.left_frame->setPalette(new_palette);
		ui.right_frame->setAutoFillBackground(true);
		ui.right_frame->setPalette(new_palette);
		ui.frame_5->setAutoFillBackground(true);
		ui.frame_5->setPalette(new_palette);
		ui.frame_7->setAutoFillBackground(true);
		ui.frame_7->setPalette(new_palette);
		ui.frame_14->setAutoFillBackground(true);
		ui.frame_14->setPalette(new_palette);
		ui.frame_15->setAutoFillBackground(true);
		ui.frame_15->setPalette(new_palette);
		ui.exch_table->setAutoFillBackground(true);
		ui.exch_table->setPalette(new_palette);

		if(QMessageBox::question(this,"Save Settings","Save the background color\nfor future sessions?") == QMessageBox::Yes)
		{
			QSettings	settings(QSettings::IniFormat,QSettings::UserScope,"ibgames", "FedTerm");
			settings.setValue("bg_colour",colour);
		}
	}
}

void	FedTerm::BuyExtras()
{
	QString	buffer("http://www.ibgames.net/fed2/extras/index.html");
	QDesktopServices::openUrl(buffer);
}

void	FedTerm::CentreMiniMap(int x,int y)						{ ui.mini_map_view->centerOn(x,y); }

void	FedTerm::ChangeFont()
{
	bool	ok;
	QFont new_font = QFontDialog::getFont(&ok,ui.output_text_edit->font(),this,"Change text font");
	if(ok)
	{
		UpdateFont(new_font);
		QSettings	settings(QSettings::IniFormat,QSettings::UserScope,"ibgames", "FedTerm");
		settings.setValue("Font",new_font);
	}
}

void	FedTerm::ClearDisplay()
{
	ui.player_list->clear();
	ui.avatar_table->clear();
	ui.system_list->clear();
}

void	FedTerm::ClearExchangeDisplay()
{
	for(int row = 0;row < Exchange::TABLE_ROWS;++row)
	{
		for(int col = 0;col < Exchange::TABLE_COLS;++col)
		{
			if(ui.exch_table->item(row,col) != nullptr)
				delete ui.exch_table->takeItem(row,col);
		}
	}
}

void	FedTerm::ClearPlanetDisplays(bool inc_planet_list)
{
	if(inc_planet_list)
		ui.planet_list->clear();
	
	ui.planet_info_edit->clear();
	ui.planet_wdf_edit->clear();
	ui.planet_build_edit->clear();
	planet_image = hyper_link_image;
	ui.planet_pic->update();
}

void	FedTerm::closeEvent(QCloseEvent *event)
{
	if(!ui.action_exit_conf->isChecked())
	{
		if(logging)
			logger->StopLogging();
		event->accept();
		return;
	}

	if(QMessageBox::question(this,"Close FedTerm","Are you sure you want to close FedTerm?") == QMessageBox::Yes)
	{
		if(logging)
			logger->StopLogging();
		event->accept();
	}
	else
		event->ignore();
}

void	FedTerm::Connect() 
{
	ClearDisplay();

	QList<int> splitter_list_2 = ui.splitter_2->sizes();	// Can't get this to work in the constructor...
	SetStatusTableWidths(splitter_list_2[0]);
	login_dialog->show();
}

void	FedTerm::ConnectionClosed()
{
	DisplayMain("Federation 2 connection closed...");
	ui.action_logon->setEnabled(true);
	ui.action_new_account->setEnabled(true);
	ui.action_logoff->setEnabled(false);
	SetStatusBar("Not connected");
}

void	FedTerm::DisplayCargoLine(const std::string& text)
{
	ui.cargo_list->addItem(QString::fromStdString(text));
}

void	FedTerm::DisplayMain(const char *text)
{
	QString	output(text);
	for(;;)
	{
		int index = output.lastIndexOf("\n");
		if(index > 0)
			output.remove(index,1);
		else
			break;
	}

	ui.output_text_edit->append(output);
	ui.output_text_edit->verticalScrollBar()->setValue(ui.output_text_edit->verticalScrollBar()->maximum());

	if(ui.main_tab->currentIndex() == 3) // The exchange tab
	{
		ui.output_text_edit2->append(output);
		ui.output_text_edit2->verticalScrollBar()->setValue(ui.output_text_edit2->verticalScrollBar()->maximum());
	}

	if(logging)
		logger->Log(text);
}

void	FedTerm::DisplayMain(const std::string& text)
{
	QString	output(QString::fromStdString(text));
	for(;;)
	{
		int index = output.lastIndexOf("\n");
		if(index > 0)
			output.remove(index,1);
		else
			break;
	}

	ui.output_text_edit->append(output);
	ui.output_text_edit->verticalScrollBar()->setValue(ui.output_text_edit->verticalScrollBar()->maximum());

	if(ui.main_tab->currentIndex() == 3) // The exchange tab
	{
		ui.output_text_edit2->append(output);
		ui.output_text_edit2->verticalScrollBar()->setValue(ui.output_text_edit2->verticalScrollBar()->maximum());
	}

	if(logging)
		logger->Log(text);
}

void	FedTerm::DisplayManifest(const std::string& text)
{
	ui.cargo_space_label->setText(QString::fromStdString(text));
	ui.cargo_list->clear();
}

// NOTE: This could be refactored, but I'm worried about the calling cost, 
// given that the event filter is called every time there is an event - AL
bool FedTerm::eventFilter(QObject *target, QEvent *event)
{
	if (event->type() == QEvent::KeyPress)
	{
		auto key = dynamic_cast<QKeyEvent *>(event)->key();
		auto input_char_count = ui.input_text_edit->toPlainText().size();
		auto input_char_count2 = ui.input_text_edit2->toPlainText().size();

		// NOTE: If no text to edit and is a keypad key, then it is a movement cmd
		if((input_char_count == 0) && (input_char_count2 == 0))
		{
			switch(key)
			{
				case Qt::Key_Home:		SendMvtCmd(Location::NW);		return true;
				case Qt::Key_End:			SendMvtCmd(Location::SW);		return true;
				case Qt::Key_Left:		SendMvtCmd(Location::WEST);	return true;
				case Qt::Key_Up:			SendMvtCmd(Location::NORTH);	return true;
				case Qt::Key_Right:		SendMvtCmd(Location::EAST);	return true;
				case Qt::Key_Down:		SendMvtCmd(Location::SOUTH);	return true;
				case Qt::Key_PageUp:		SendMvtCmd(Location::NE);		return true;
				case Qt::Key_PageDown:	SendMvtCmd(Location::SE);		return true;
			}
		}

		// No need to check size of input - server handles it
		if(target == ui.input_text_edit)
		{
			if(key == Qt::Key_Return)
			{
				if(input_char_count > 0)
				{
					QString	input(ui.input_text_edit->document()->toPlainText());
					QString	to_send = input + '\n';
					comms->Write(to_send);
					input.insert(0,"\n>");
					DisplayMain(input.toStdString().c_str());
					ui.input_text_edit->clear();
				}
				return true;
			}
		}

		if(target == ui.input_text_edit2)
		{
			if(key == Qt::Key_Return)
			{
				if(input_char_count2 > 0)
				{
					QString	input(ui.input_text_edit2->document()->toPlainText());
					QString	to_send = input + '\n';
					comms->Write(to_send);
					input.insert(0,"\n>");
					DisplayMain(input.toStdString().c_str());
					ui.input_text_edit2->clear();
				}
				return true;
			}
		}

		if((key >= Qt::Key_F1) && (key <= Qt::Key_F12))
		{
			QString	macro(user_macros->GetMacro(static_cast<Qt::Key>(key)));
			if(macro.size() > 0)
			{
				comms->Write(macro);
				// Turn multi-line macros into a single '//' separated line
				macro.replace('\n',"//",Qt::CaseInsensitive);
				macro.remove(macro.length() -2,2);
				QString	scr_macro("\n>" + macro);
				DisplayMain(scr_macro.toStdString());
			}
			return true;
		}

	} // End of keypress processing

	if(event->type() == QEvent::Paint)
	{
		if(target == ui.avatar_pic)
		{
			QPainter	painter(ui.avatar_pic);
			painter.drawImage(0,0,avatar_image);
			return true;
		}

		if(target == ui.players_pic)
		{
			QPainter	painter(ui.players_pic);
			painter.drawImage(0,0,players_image);
			return true;
		}

		if(target == ui.ship_pic)
		{
			QPainter	painter(ui.ship_pic);
			painter.drawImage(0,0,ship_image);
			return true;
		}

		if(target == ui.ship_pic_2)
		{
			QPainter	painter(ui.ship_pic_2);
			painter.drawImage(0,0,ship_image);
			return true;
		}

		if(target == ui.planet_pic)
		{
			QPainter	painter(ui.planet_pic);
			painter.drawImage(0,0,planet_image);
			return true;
		}

		if(target == ui.exchange_pic)
		{
			QPainter	painter(ui.exchange_pic);
			painter.drawImage(0,0,exchange_image);
			return true;
		}
	}	// End of paint processing

	return(QWidget::eventFilter(target, event));
}

void	FedTerm::ExchangeCopy()
{
	QSettings	settings(QSettings::IniFormat,QSettings::UserScope,"ibgames", "FedTerm");
	if(ui.action_copy_all_exch->isChecked())
		settings.setValue("exch_copy",true);
	else
		settings.setValue("exch_copy",false);
}

void	FedTerm::ExchangeTableDClicked(int row,int col)
{
	QString	commod_name(ui.exch_table->verticalHeaderItem(row)->text());
	QString	command;

	if(col == 2)
		command = "check price ";
	if((col == 0) && (ui.exch_table->item(row,col) != nullptr))
	{
		if(ui.exch_table->item(row,0)->text().contains("Buys"))
			command = "sell ";
		else
			command = "buy ";
	}

	command += commod_name + "\n";
	comms->Write(command);
	comms->Write("<c-send-manifest>\n");
	command.prepend("\n>");
	DisplayMain((command).toStdString());
}

void	FedTerm::ExitProgram()	{ close(); }	// See also closeEvent() override

void	FedTerm::Feedback()
{
	QString	buffer("http://www.ibgames.net/contact");
	QDesktopServices::openUrl(buffer);
}

int	FedTerm::FindStatRow(const std::string& stat_name)
{
	QString	name(QString::fromStdString(stat_name));
	int		rows = ui.avatar_table->rowCount();
	
	for(int count = 0;count < rows;++count)
	{
		if(ui.avatar_table->item(count,StatTable::NAME_COL) == 0) // unused line
			continue;
		if(ui.avatar_table->item(count,StatTable::NAME_COL)->text() == name)
			return count;
	}

	return StatTable::UNKNOWN_ROW;
}

void	FedTerm::FtManual()
{
	QString	buffer("http://www.ibgames.com/fed2/fedterm/manual/index.html");
	QDesktopServices::openUrl(buffer);
}

void	FedTerm::GameManual()
{
	QString	buffer("http://www.ibgames.com/fed2/guide/index.html");
	QDesktopServices::openUrl(buffer);
}

void	FedTerm::HideLoginDialog()		{ login_dialog->hide(); }
void	FedTerm::HideNewbieDialog()	{ newbie_dialog->hide(); }

void	FedTerm::HideMovementArrows()
{
	if(ui.action_hide_mvt->isChecked())
		ui.arrows_frame->hide();
	else
		ui.arrows_frame->show();
}

void	FedTerm::LogAll()
{
	QSettings	settings(QSettings::IniFormat,QSettings::UserScope,"ibgames", "FedTerm");
	if(ui.action_log_all->isChecked())
	{
		StartLogging();
		settings.setValue("auto_logging",true);
	}
	else
		settings.setValue("auto_logging",false);	// Don't stop any existing logging
}

void	FedTerm::LoginInfoReady()	{ comms->ConnectToServer(login_rec); }
void	FedTerm::LogOff()				{ comms->LogOff(); }

void	FedTerm::MainTabChanged(int index)
{
	if((index == 2) && (ui.system_list->count() == 0))
		comms->Write("<c-send-system-links/>\n");

	if(index == 3)
		ui.output_text_edit2->clear();
}

FedTerm *FedTerm::MainWindow()	{ return main_window; }

bool	FedTerm::MakeDirectory(const std::string& path)
{
	QString temp(QString::fromStdString(path));
	QDir	dir;
	return(dir.mkpath(temp));
}

void	FedTerm::MapsHelp()
{
	QString	buffer("http://www.ibgames.com/fed2/maps/planetmaps.html");
	QDesktopServices::openUrl(buffer);
}

void	FedTerm::Message(const QString& title,const QString& text)	{ QMessageBox::warning(this,title,text); }

void	FedTerm::Move(bool)
{
	QString	direction;
	if(ui.n_btn == sender())		direction = "north\n";
	if(ui.ne_btn == sender())		direction = "ne\n";
	if(ui.e_btn == sender())		direction = "east\n";
	if(ui.se_btn == sender())		direction = "se\n";
	if(ui.s_btn == sender())		direction = "south\n";
	if(ui.sw_btn == sender())		direction = "sw\n";
	if(ui.w_btn == sender())		direction = "west\n";
	if(ui.nw_btn == sender())		direction = "nw\n";
	if(ui.up_btn == sender())		direction = "up\n";
	if(ui.down_btn == sender())	direction = "down\n";
	if(ui.board_btn == sender())	direction = "board\n";

	comms->Write(direction);
	direction.prepend("\n>");
	DisplayMain(direction.toStdString());

}

void	FedTerm::NewbieConnect()
{
	ClearDisplay();
	newbie_dialog->show();
}

void	FedTerm::NewbieInfoReady()
{
	NewbieRec *newbie_rec	= newbie_dialog->GetNewbieRec();
	comms->ConnectToServer(newbie_rec);
}

void	FedTerm::ExitConf()
{
	QSettings	settings(QSettings::IniFormat,QSettings::UserScope,"ibgames", "FedTerm");
	if(ui.action_exit_conf->isChecked())
		settings.setValue("exit_conf",true);
	else
		settings.setValue("exit_conf",false);
}

void	FedTerm::PlanetListClicked(QListWidgetItem *item)
{
	ClearPlanetDisplays(false);
	QString	planet(item->text());
	QString	text("<c-planet-info name='" + planet + "'/>\n");
	comms->Write(text);
}

void	FedTerm::PlayerListClicked(QListWidgetItem *item)
{
	QString	text("spynet report " + item->text() +"\n");
	comms->Write(text);
	text.prepend("\n>");
	DisplayMain(text.toStdString());
}

void	FedTerm::PlaySound(int sound) 
{ 
	switch(sound)
	{
		case SoundFx::ALERT:	
				if(!ui.action_text_alert->isChecked())	
					return;
				break;
		case SoundFx::FRIENDS:	break;
		default: return;
	}

	sound_fx->Play(sound);
}

void	FedTerm::ProvideNewAvatarName()
{
	new_name_dialog->show();
}

void	FedTerm::QuickStartGuide()
{
	QString	buffer("http://www.ibgames.com/fed2/manuals/quick.html");
	QDesktopServices::openUrl(buffer);
}

void	FedTerm::RemovePlayer(const std::string& name)
{
	QList<QListWidgetItem *> items = ui.player_list->findItems(QString::fromStdString(name),Qt::MatchFixedString);
	if(items.empty())
		return;
	else 
		delete items[0];
}

void	FedTerm::resizeEvent(QResizeEvent *event)
{
	QList<int> splitter_list_2 = ui.splitter_2->sizes();
	SetStatusTableWidths(splitter_list_2[0]);
}

void	FedTerm::SaveLoginRec()
{
	if(login_rec->avatar_name == "")	// must have been a new avatar...
		return;

	QSettings	settings(QSettings::IniFormat,QSettings::UserScope,"ibgames", "FedTerm");
	settings.beginGroup("Avatars");
	settings.setValue("current_avatar",login_rec->avatar_name);
	settings.beginGroup(login_rec->avatar_name);
	settings.setValue("host_name",login_rec->host);
	settings.setValue("port",static_cast<unsigned int>(login_rec->port));
	settings.setValue("name",login_rec->avatar_name);
	settings.setValue("account_id",login_rec->account_id);
	if(login_rec->save_pwd)
		settings.setValue("password",login_rec->password);
	else
		settings.setValue("password","");
	settings.endGroup(); // Avatar name
	settings.endGroup(); // "Avatars"
	login_dialog->hide();
}

void	FedTerm::SaveLoginRec(const NewbieRec& rec)
{
	QSettings	settings(QSettings::IniFormat,QSettings::UserScope,"ibgames", "FedTerm");
	settings.beginGroup("Avatars");
	settings.setValue("current_avatar",rec.name);
	settings.beginGroup(rec.name);
	settings.setValue("host_name",rec.server_name);
	settings.setValue("port",static_cast<unsigned int>(rec.port_num));
	settings.setValue("name",rec.name);
	settings.setValue("account_id",rec.account_id);
	if(rec.save_pw)
		settings.setValue("password",rec.pwd);
	else
		settings.setValue("password","");
	settings.endGroup(); // Avatar name
	settings.endGroup(); // "Avatars"
	newbie_dialog->hide();
}

void	FedTerm::SaveScreenBuffer()
{
	QString	file_name = QFileDialog::getSaveFileName(this, tr("Save Screen Buffer"),
				QString(),tr("All Files (*.log)"));
	if(file_name == "")
		return;
	std::ofstream	file(file_name.toStdString());
	if(!file)
	{
		Message("Save screen buffer","Unable to open file");
		return;
	}
	file << ui.output_text_edit->toPlainText().toStdString();
	file.close();
	Message("Save screen buffer","Screen buffer saved to file");
}

void	FedTerm::SendCmd(const QString command)
{
	QString	cmd(command);
	comms->Write(cmd);
	if(command[0] != '<')
	{
		cmd.insert(0,"\n>");
		DisplayMain(cmd.toStdString().c_str());
	}
}

void	FedTerm::SendMvtCmd(int direction)
{
	QString	cmd;
	switch(direction)
	{
		case Location::NORTH:	cmd = "north\n";	break;
		case Location::NE:		cmd = "ne\n";		break;
		case Location::EAST:		cmd = "east\n";	break;
		case Location::SE:		cmd = "se\n";		break;
		case Location::SOUTH:	cmd = "south\n";	break;
		case Location::SW:		cmd = "sw\n";		break;
		case Location::WEST:		cmd = "west\n";	break;
		case Location::NW:		cmd = "nw\n";		break;
		case Location::UP:		cmd = "up\n";		break;
		case Location::DOWN:		cmd = "down\n";	break;
		default:		return;
	}

	comms->Write(cmd);
	cmd.insert(0,"\n>");
	DisplayMain(cmd.toStdString().c_str());
}

void	FedTerm::SetAvatarName(const std::string& name)			
{ 
	QString	text(QString::fromStdString(name));
	ui.avatar_name_label->setText(text);
	// Reset the title this way - MainWindow::windowTitle() truncates the title...
	QString win_title = "FedTerm version " + QString::fromStdString(version);
	win_title += " " + text;
	setWindowTitle(win_title);
}

void	FedTerm::SetConnectDisplay()
{
	ui.action_logon->setEnabled(false);
	ui.action_new_account->setEnabled(false);
	ui.action_logoff->setEnabled(true);
	SetStatusBar("Connected");
}

void	FedTerm::SetInfoBar(const std::string& new_info)		{ ui.info_label->setText(QString::fromStdString(new_info)); }
void	FedTerm::SetInfraScrollBar()	{ ui.planet_build_edit->verticalScrollBar()->setValue(ui.planet_build_edit->verticalScrollBar()->minimum()); }
void	FedTerm::SetInputFocus()										{ ui.input_text_edit->setFocus(); }
void	FedTerm::SetLocInfo(const std::string& loc_info)		{ ui.loc_label->setText(QString::fromStdString(loc_info)); }

void	FedTerm::SetStat(const std::string& name,const std::string& value,int row)
{
	QTableWidgetItem *name_item = ui.avatar_table->item(row,StatTable::NAME_COL);
	if(name_item == 0)
		name_item = new QTableWidgetItem(QString::fromStdString(name));
	else
		name_item->setText(QString::fromStdString(name));
	ui.avatar_table->setItem(row,StatTable::NAME_COL,name_item);

	QTableWidgetItem *value_item = ui.avatar_table->item(row,StatTable::VALUE_COL);
	if(value_item == 0)
		value_item = new QTableWidgetItem(QString::fromStdString(value));
	else
		value_item->setText(QString::fromStdString(value));
	ui.avatar_table->setItem(row,StatTable::VALUE_COL,value_item);
}

void	FedTerm::SetStatusBar(const std::string& new_status)	{ ui.status_label->setText(QString::fromStdString(new_status)); }

void	FedTerm::SetStatusTableWidths(int frame_width)
{
	int delta = frame_width - STATUS_BOX_MIN_WIDTH;
	if(delta < 2)
		delta = 0;
	ui.avatar_table->resize(STATUS_TABLE_MIN_WIDTH + delta,ui.avatar_table->size().height());
	ui.avatar_table->setColumnWidth(0,STATUS_TABLE_MIN_COL0_WIDTH + delta/2);
	ui.avatar_table->setColumnWidth(1,STATUS_TABLE_MIN_COL1_WIDTH + delta/2);
}

void	FedTerm::SetTitle(const std::string& title)				{ setWindowTitle(QString::fromStdString(title)); }

void	FedTerm::SetUpExchangeTable()
{
	for(auto count = 0;Exchange::commodity_names[count] != "";++count)
	{
		QTableWidgetItem	*item = new QTableWidgetItem(QString::fromStdString(Exchange::commodity_names[count]));
		ui.exch_table->setVerticalHeaderItem(count,item);
		ui.exch_table->setRowHeight(count,24);
	}
	QStringList	headers;
	headers << "Buy/Sell" << "Stock(tons)" << "Price/Ton";
	ui.exch_table->setHorizontalHeaderLabels(headers);
	ui.exch_table->setColumnWidth(0,80);
	ui.exch_table->setColumnWidth(1,87);
	ui.exch_table->setColumnWidth(2,87);
}

void	FedTerm::SetUpMenuConnections()
{
	connect(ui.action_about,SIGNAL(triggered()),this,SLOT(About()));
	connect(ui.action_auto_exch,SIGNAL(triggered()),this,SLOT(AutoExchangeSwitch()));
	connect(ui.action_background,SIGNAL(triggered()),this,SLOT(BackgroundColour()));
	connect(ui.action_buy_extras,SIGNAL(triggered()),this,SLOT(BuyExtras()));
	connect(ui.action_copy_all_exch,SIGNAL(triggered()),this,SLOT(ExchangeCopy()));
	connect(ui.action_exit,SIGNAL(triggered()),this,SLOT(ExitProgram()));
	connect(ui.action_feedback, SIGNAL(triggered()),this,SLOT(Feedback()));
	connect(ui.action_fkeys,SIGNAL(triggered()),this,SLOT(ShowFkeys()));
	connect(ui.action_fonts,SIGNAL(triggered()),this,SLOT(ChangeFont()));
	connect(ui.action_friends,SIGNAL(triggered()),this,SLOT(ShowFriends()));
	connect(ui.action_ft_manual,SIGNAL(triggered()),this,SLOT(FtManual()));
	connect(ui.action_fed_manual,SIGNAL(triggered()),this,SLOT(GameManual()));
	connect(ui.action_hide_mvt,SIGNAL(triggered()),this,SLOT(HideMovementArrows()));
	connect(ui.action_log_all,SIGNAL(triggered()),this,SLOT(LogAll()));
	connect(ui.action_logon,SIGNAL(triggered()),this,SLOT(Connect()));
	connect(ui.action_logoff,SIGNAL(triggered()),this,SLOT(LogOff()));
	connect(ui.action_maps,SIGNAL(triggered()),this,SLOT(MapsHelp()));
	connect(ui.action_new_account,SIGNAL(triggered()),this,SLOT(NewbieConnect()));
	connect(ui.action_exit_conf,SIGNAL(triggered()),this,SLOT(ExitConf()));
	connect(ui.action_quick_start,SIGNAL(triggered()),this,SLOT(QuickStartGuide()));
	connect(ui.action_start_logging,SIGNAL(triggered()),this,SLOT(StartLogging()));
	connect(ui.action_stats,SIGNAL(triggered(bool)),this,SLOT(StatsPanel(bool)));
	connect(ui.action_stop_logging,SIGNAL(triggered()),this,SLOT(StopLogging()));
	connect(ui.action_save_buffer,SIGNAL(triggered()),this,SLOT(SaveScreenBuffer()));
	connect(ui.action_text_alert,SIGNAL(triggered()),this,SLOT(TextAlert()));
	connect(ui.action_tmc_voting,SIGNAL(triggered()),this,SLOT(TMCVoting()));
}

void	FedTerm::SetUpMovementConnections()
{
	connect(ui.n_btn,SIGNAL(clicked(bool)),this,SLOT(Move(bool)));
	connect(ui.ne_btn,SIGNAL(clicked(bool)),this,SLOT(Move(bool)));
	connect(ui.e_btn,SIGNAL(clicked(bool)),this,SLOT(Move(bool)));
	connect(ui.se_btn,SIGNAL(clicked(bool)),this,SLOT(Move(bool)));
	connect(ui.s_btn,SIGNAL(clicked(bool)),this,SLOT(Move(bool)));
	connect(ui.sw_btn,SIGNAL(clicked(bool)),this,SLOT(Move(bool)));
	connect(ui.w_btn,SIGNAL(clicked(bool)),this,SLOT(Move(bool)));
	connect(ui.nw_btn,SIGNAL(clicked(bool)),this,SLOT(Move(bool)));
	connect(ui.up_btn,SIGNAL(clicked(bool)),this,SLOT(Move(bool)));
	connect(ui.down_btn,SIGNAL(clicked(bool)),this,SLOT(Move(bool)));
	connect(ui.board_btn,SIGNAL(clicked(bool)),this,SLOT(Move(bool)));
}


void	FedTerm::ShowFriends()	{ comms->ShowFriends(); }

void	FedTerm::ShowFkeys()		{ user_macros->show(); }

void	FedTerm::StartCommodityTicker(const std::string& text)
{
	ticker_text = QString::fromStdString(text);
	ticker_index = 1;
	ticker_timer->start(80);
}

void	FedTerm::Splitter2Moved(int,int)
{
	QList<int> splitter_list_2 = ui.splitter_2->sizes();
	SetStatusTableWidths(splitter_list_2[0]);
}

void	FedTerm::StartLogging()
{
	if(logging)
		return;

	if(logger->StartLog())
	{
		logging = true;
		ui.logging_label->setText("Logging...");
		ui.action_start_logging->setEnabled(false);
		ui.action_stop_logging->setEnabled(true);
	}
}

QString	FedTerm::StartMacro()	{ return(user_macros->GetStartupCmds()); }

void	FedTerm::StatsPanel(bool checked)
{
	if(checked)
		ui.left_frame->hide();
	else
		ui.left_frame->show();

	QSettings	settings(QSettings::IniFormat,QSettings::UserScope,"ibgames", "FedTerm");
	settings.setValue("Stats Panel",checked);
}

void	FedTerm::StopLogging()
{
	if(logging)
	{
		logger->StopLogging();
		logging = false;
		ui.logging_label->setText("");
		ui.action_start_logging->setEnabled(true);
		ui.action_stop_logging->setEnabled(false);
	}
}

void	FedTerm::Switch2Exchange()
{
	if(ui.action_auto_exch->isChecked())
		ui.main_tab->setCurrentIndex(3);
}

void	FedTerm::SystemListClicked(QListWidgetItem *item)
{
	ClearPlanetDisplays(true);
	QString	star(item->text());
	QString	text("<c-planets name='" + star + "'/>\n");
	comms->Write(text);
}

void	FedTerm::TextAlert()
{
	QSettings	settings(QSettings::IniFormat,QSettings::UserScope,"ibgames", "FedTerm");
	settings.setValue("Incoming Text Alert",ui.action_text_alert->isChecked());
	comms->WantTextAlerts(ui.action_text_alert->isChecked());
}

void	FedTerm::TMCVoting()
{
	QString	buffer("http://www.mudconnect.com/cgi-bin/vote_rank.cgi?mud=Federation+II");
	QDesktopServices::openUrl(buffer);
}

void	FedTerm::UpdateAvatarPic(const std::string& name)
{
	ui.avatar_name->setText(QString::fromStdString(name));
	std::ostringstream	buffer;
	buffer << base_dir << "/pictures/";
	buffer << name << ".png";
	QFile file(QString::fromStdString(buffer.str()));
	if(file.exists())
		avatar_image = QImage(QString::fromStdString(buffer.str()));
	else
		avatar_image = default_image;
	ui.avatar_pic->update();
}

void	FedTerm::UpdateCommodityName(const std::string& name)				{ 	ui.exch_commod_label->setText(QString::fromStdString(name)); }

void	FedTerm::UpdateCommodityPic(const std::string& name)
{
	std::ostringstream	buffer;
	buffer << base_dir << "/exchanges/";
	buffer << name << ".png";
	exchange_image = QImage(QString::fromStdString(buffer.str()));
	ui.exchange_pic->update();
}

void	FedTerm::UpdateCommodityPrice(const std::string& price)			{ ui.exch_price_label->setText(QString::fromStdString(price)); }
void	FedTerm::UpdateCommodityQuantity(const std::string& quantity)	{ ui.exch_quantity_label->setText(QString::fromStdString(quantity)); }

void	FedTerm::UpdateCommodityTicker()
{
	if(ticker_index <= ticker_text.size())
		ui.exch_ticker_label->setText(ticker_text.left(ticker_index++));
	else
		ticker_timer->stop();
}

void	FedTerm::UpdateExchangeLine(int row,const std::string& buy_sell,const std::string& stock,const std::string& price)
{
	ui.exch_table->setItem(row,0,new QTableWidgetItem(QString::fromStdString(buy_sell)));
	ui.exch_table->item(row,0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
	ui.exch_table->setItem(row,1,new QTableWidgetItem(QString::fromStdString(stock)));
	ui.exch_table->item(row,1)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
	ui.exch_table->setItem(row,2,new QTableWidgetItem(QString::fromStdString(price)));
	ui.exch_table->item(row,2)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
}

void	FedTerm::UpdateFont(const QFont new_font)
{
		// Main tab
		ui.output_text_edit->setFont(new_font);
		ui.input_text_edit->setFont(new_font);
		ui.avatar_table->setFont(new_font);
		ui.player_list->setFont(new_font);
		ui.avatar_name->setFont(new_font);
		ui.players_name->setFont(new_font);
		ui.avatar_name_label->setFont(new_font);

		// Star systems tab
		ui.system_list->setFont(new_font);
		ui.planet_list->setFont(new_font);
		ui.planet_info_edit->setFont(new_font);
		ui.planet_wdf_edit->setFont(new_font);
		ui.planet_build_edit->setFont(new_font);

		// Exchanges tab
		ui.output_text_edit2->setFont(new_font);
		ui.input_text_edit2->setFont(new_font);
		ui.cargo_list->setFont(new_font);
		ui.cargo_space_label->setFont(new_font);
}

void	FedTerm::UpdatePlanetPic(const std::string& name)
{
	std::ostringstream	buffer;
	buffer << base_dir + "/planets/";
	buffer << name << ".png";
	QFile file(QString::fromStdString(buffer.str()));
	if(file.exists())
		planet_image = QImage(QString::fromStdString(buffer.str()));
	else
		planet_image = hyper_link_image;
	ui.planet_pic->update();
}

void	FedTerm::UpdatePlayerNumBar()
{
	static const QString text(" players in the game");

	int num = ui.player_list->count();
	if(num == 0)
	{
		ui.players_label->setText("");
		return;
	}

	ui.players_label->setText(QString::number(num) += text);
}

void	FedTerm::UpdatePlayersPic(const std::string& name)
{
	ui.players_name->setText(QString::fromStdString(name));
	std::ostringstream	buffer;
	buffer << base_dir << "/pictures/";
	buffer << name << ".png";
	QFile file(QString::fromStdString(buffer.str()));
	if(file.exists())
		players_image = QImage(QString::fromStdString(buffer.str()));
	else
		players_image = default_image;
	ui.players_pic->update();
}

void	FedTerm::UpdateShipPic(const std::string& name)
{
	std::ostringstream	buffer;
	buffer << base_dir << "/pictures/";
	buffer << name << ".png";
	ship_image = QImage(QString::fromStdString(buffer.str()));
	ui.ship_pic->update();
}


/* ----------------- Work in Progress  ----------------- */ 

