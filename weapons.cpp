/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#include "weapons.h"

#include <sstream>

#include "fedterm.h"
#include "stat_table.h"
#include "xml_parser.h"


const std::string	Weapons::xml_stat_names[] = 
	{ "missiles", "rack", "laser", "tl", "ql", "" }; 
const std::string	Weapons::display_names[] =  
	{ "Missiles","Missile Rack", "Laser", "Twin Laser", "Quad Laser", "" };

const WeaponInfo	Weapons::empty_slot(std::make_pair("",""));


Weapons::Weapons()
{
	Clear();
}


void Weapons::Clear()
{
	for(auto& elem : weapon_slots)
		elem = empty_slot;
	current_start = 0;
	slot_index = 0;
}

int	Weapons::EndUpdate(int start_index)
{
	if(start_index == WRITE_LATER)
		return start_index;

	int index =start_index;
	if(start_index == CURRENT_START)
		index = current_start;
	else
		current_start = start_index;

	if((missiles == "") && (weapon_slots[0].first == ""))
		return start_index;

	FedTerm::MainWindow()->SetStat("  ", "  ", index++);
	FedTerm::MainWindow()->SetStat("  ~ Weapons ~","  ",index++);
	if(missiles != "")
		FedTerm::MainWindow()->SetStat(display_names[MISSILES],missiles,index++);

	for(int count = 0;count < MAX_WEAPON_SLOTS;++count)
	{
		if(weapon_slots[count] == empty_slot)
			return index;
		else
			FedTerm::MainWindow()->SetStat(weapon_slots[count].first,weapon_slots[count].second,index++);
	}

	return index;
}

void Weapons::MakeWeaponAndPercentage(const AttribList& attribs,int name_index)
{
	int percent = XmlParser::FindNumAttrib(attribs,"cur",UNKNOWN_VAL);
	if(percent == UNKNOWN_VAL)
		return;
	
	std::ostringstream	buffer;
	buffer << percent << "%";
	weapon_slots[slot_index++] = std::make_pair(display_names[name_index],buffer.str());
}
	
void	Weapons::ProcessInfo(const AttribList& attribs)
{
	std::string	stat;
	XmlParser::FindAttrib(attribs,"stat",stat);
	int	name_index = UNKNOWN_VAL;
	for(auto count = 0;xml_stat_names[count] != "";++count)
	{
		if(xml_stat_names[count] == stat)
		{
			name_index = count;
			break;
		}
	}
	if(name_index == UNKNOWN_VAL)
		return;

	if(name_index == MISSILES)
		StatTable::MakeBiValueStat(attribs,missiles);
	else
		MakeWeaponAndPercentage(attribs,name_index);
}

void	Weapons::StartUpdate()
{
	for(auto& elem : weapon_slots)
		elem = empty_slot;
	missiles = "";
	slot_index = 0;
}


/* ---------------- Work in Progress ---------------- */

