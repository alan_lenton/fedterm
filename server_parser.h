/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#ifndef SERVERPARSER_H
#define SERVERPARSER_H

#include "xml_parser.h"

class Avatar;
class Exchange;
class FedMap;
class Friends;
class Planet;
class StatTable;

class ServerParser : public XmlParser
{
private:
	static const int	NOT_AN_ELEMENT = -1;
	static const std::string	elements[];

	enum TextStates
	{
		CLEAR, DEFAULT, ADD_PLAYER, REMOVE_PLAYER, ADD_SYSTEM, ADD_PLANET, 
		PLANET_INFO, PLANET_WARE, PLANET_DEPOT, PLANET_FACTORY, INFRA_BUILD,
		EXCH_START, EXCH_SELL, EXCH_BUY
	};

	Avatar		*avatar;
	Friends		*friends;
	FedMap		*fed_map;
	Planet		*planet;
	Exchange		*exchange;
	TextStates	status;
	bool			want_text_alerts;

	int	FindElement(const std::string& elem);

	void	AddPlanet(const AttribList& attribs);
	void	AddPlayer(const AttribList& attribs);
	void	AddSystem(const AttribList& attribs);
	void	CargoManifest(const AttribList& attribs);
	void	ExchangeBuy(const AttribList& attribs);
	void	ExchangeSell(const AttribList& attribs);
	void	ExchangeStart(const AttribList& attribs);
	void	GeneralPlanetInfo(const AttribList& attribs);
	void	Message();
	void	PlanetDepots(const AttribList& attribs);
	void	PlanetFactories(const AttribList& attribs);
	void	PlanetInfraBuilds(const AttribList& attribs);
	void	PlanetWarehouses(const AttribList& attribs);
	void	RemovePlayer(const AttribList& attribs);
	void	Spynet(const AttribList& attribs);

	virtual void	StartElement(const std::string& elem,const AttribList& attribs);
	virtual void	EndElement(const std::string& elem);
	virtual void	TextData(const std::string& text);

public:
	ServerParser();
	~ServerParser();

	void	Reset()							{ status = CLEAR;	 }
	void	SetMap(FedMap *f_map)		{ fed_map = f_map; }
	void	ShowFriends();
	void	WantTextAlerts(bool which)	{ want_text_alerts = which; } 
};

#endif
