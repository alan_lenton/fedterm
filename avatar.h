/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#ifndef AVATAR_H
#define AVATAR_H

#include <list>
#include <string>

/* NOTE: In the code the Avatar/avatar is material relating to the player
	using FedTerm, Player/player normally refers to other players in the game. */

class StatTable;
class WeaponStats;

using AttribList = std::list<std::pair<std::string,std::string>>;

class Avatar
{
private:
	static const int	MAX_WEAPONS = 4;
	static const int	UNKNOWN_VAL = -1;

	std::string	name;		
	std::string rank;

	StatTable		*stat_table;
	WeaponStats		*weapon_stats;

public:
	Avatar();
	~Avatar();

	const std::string&	Name()	{ return name; }

	void	FullStats(const AttribList& attribs);
	void	AvatarInfo(const AttribList& attribs);
	void	ComputerInfo(const AttribList& attribs);
	void	ShipInfo(const AttribList& attribs);
	void	WeaponInfo(const AttribList& attribs);
	void	Weapons(const AttribList& attribs);
};

#endif


