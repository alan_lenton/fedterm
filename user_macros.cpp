/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#include "user_macros.h"

#include <QPainter>
#include <QSettings>

const QString	UserMacros::macro_labels[MAX_MACROS] =
{
	"Startup Commands","Text for F1-Key", "Text for F2-Key", "Text for F3-Key", 
	"Text for F4-Key", "Text for F5-Key", "Text for F6-Key", "Text for F7-Key", 
	"Text for F8-Key", "Text for F9-Key", "Text for F10-Key","Text for F11-Key",
	"Text for F12-Key"
};

const QString	UserMacros::instructions("You can assign up to 400 characters to each F-Key.\
 Type one command per line. The commands will be displayed on screen and sent when you press\
 the F-Key. The 'Start Up' commands will be sent to the server automatically when you log on.");

UserMacros::UserMacros(QWidget *parent) : QWidget(parent)
{
	ui.setupUi(this);

	picture = QImage(":/FedTerm/Images/f_keys.png");
	ui.image_widget->installEventFilter(this);
	ui.text_edit->installEventFilter(this);

	ui.instruct_edit->setPlainText(instructions);

	current_macro = START_CMDS;
	ui.f_key_label->setText(macro_labels[current_macro]);
	LoadMacros();

	connect(ui.cancel_btn,SIGNAL(clicked()),this,SLOT(ScrapChanges()));
	connect(ui.last_btn,SIGNAL(clicked()),this,SLOT(LastMacro()));
	connect(ui.next_btn,SIGNAL(clicked()),this,SLOT(NextMacro()));
	connect(ui.ok_btn,SIGNAL(clicked()),this,SLOT(SaveAndExit()));
}

UserMacros::~UserMacros()
{

}

void	UserMacros::ChangeMacro()
{
	ui.f_key_label->setText(macro_labels[current_macro]);
	ui.last_btn->setEnabled(current_macro > 0);
	ui.next_btn->setEnabled(current_macro != (MAX_MACROS - 1));

	ui.text_edit->clear();
	ui.text_edit->setPlainText(macros[current_macro]);
}

bool UserMacros::eventFilter(QObject *target, QEvent *event)
{
	if((target == ui.image_widget) && (event->type() == QEvent::Paint))
	{
		QPainter	painter(ui.image_widget);
		painter.drawImage(0, 0, picture);
		return true;
	}

	if (event->type() == QEvent::KeyPress)
	{
		auto char_count = ui.text_edit->document()->characterCount();
		if(char_count > MAX_MACRO_SIZE)
			return true;
	}
	return(QWidget::eventFilter(target, event));
}

QString UserMacros::GetMacro(Qt::Key key)
{
	switch(key)
	{
		case Qt::Key_F1:	return macros[1];
		case Qt::Key_F2:	return macros[2];
		case Qt::Key_F3:	return macros[3];
		case Qt::Key_F4:	return macros[4];
		case Qt::Key_F5:	return macros[5];
		case Qt::Key_F6:	return macros[6];
		case Qt::Key_F7:	return macros[7];
		case Qt::Key_F8:	return macros[8];
		case Qt::Key_F9:	return macros[9];
		case Qt::Key_F10:	return macros[10];
		case Qt::Key_F11:	return macros[11];
		case Qt::Key_F12:	return macros[12];
		default:				return "";
	}
}

void	UserMacros::LastMacro()
{
	if(current_macro != START_CMDS)
	{
		macros[current_macro] = ui.text_edit->toPlainText();
		if((macros[current_macro].size() > 0) && !macros[current_macro].endsWith('\n'))
			macros[current_macro].append('\n');
		--current_macro;
		ChangeMacro();
	}
}

void	UserMacros::LoadMacros()
{
	QSettings	settings(QSettings::IniFormat,QSettings::UserScope,"ibgames", "FedTerm");
	settings.beginGroup("UserMacros");
	for(int count = 0;count < MAX_MACROS;++count)
	{
		macros[count] = settings.value(macro_labels[count],"").toString();
		if(!macros[count].endsWith("\n"))
			macros[count].append("\n");
	}
	settings.endGroup();

	ui.text_edit->clear();
	ui.text_edit->setPlainText(macros[current_macro]);
}

void	UserMacros::NextMacro()
{
	if(current_macro < (MAX_MACROS - 1))
	{
		macros[current_macro] = ui.text_edit->toPlainText();
		if((macros[current_macro].size() > 0) && !macros[current_macro].endsWith('\n'))
			macros[current_macro].append('\n');
		++current_macro;
		ChangeMacro();
	}
}

void	UserMacros::SaveAndExit()
{
	macros[current_macro] = ui.text_edit->toPlainText();
	SaveMacros();
	hide();
}

void	UserMacros::SaveMacros()
{
	QSettings	settings(QSettings::IniFormat,QSettings::UserScope,"ibgames","FedTerm");

	settings.beginGroup("UserMacros");
	for(int count = 0;count < MAX_MACROS;++count)
		settings.setValue(macro_labels[count],macros[count]);
	settings.endGroup();
}

void	UserMacros::ScrapChanges()
{
	LoadMacros();
	hide();
}

