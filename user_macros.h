/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#ifndef USER_MACROS_H
#define USER_MACROS_H

#include <QWidget>

#include "ui_user_macros.h"

class UserMacros : public QWidget
{
	Q_OBJECT

private:
	Ui::UserMacros ui;

	static const int		MAX_MACROS = 13;	// Startup + 10 F-keys
	static const int		START_CMDS = 0;
	static const int		MAX_MACRO_SIZE = 400;
	static const QString	macro_labels[MAX_MACROS];
	static const QString	instructions;

	QString	macros[MAX_MACROS];
	QImage	picture;

	int	current_macro;

	bool	eventFilter(QObject *target, QEvent *event);

	void	ChangeMacro();
	void	LoadMacros();
	void	SaveMacros();

private slots:
	void	LastMacro();
	void	NextMacro();
	void	SaveAndExit();
	void	ScrapChanges();

public:
	UserMacros(QWidget *parent = 0);
	~UserMacros();

	QString	GetMacro(Qt::Key key);
	QString	GetStartupCmds()		{ return macros[0]; }
};

#endif // USER_MACROS_H
