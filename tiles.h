/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#ifndef TILES_H
#define TILES_H

#include <QPixmap>
#include <QString>

class Tiles
{
public:
	static const int HEIGHT = 16;
	static const int WIDTH = 16;
	static const int NO_TILE = -1;
	enum
	{
		UNUSED, USED, SELECTED, BAD, HIDDEN, NORTH,
		NE, EAST, SE, SOUTH, SW, WEST, NW, CURRENT, 
		LINK, PEACE, NUM_TILES
	};

private:
	static const QString	pix_files[];

	QPixmap	*pix_maps[NUM_TILES];

public:
	Tiles();
	~Tiles();

	QPixmap	*Tile(int which)				{ return pix_maps[which]; }
	qint64	GetCacheKey(int tile_num)	{ return pix_maps[tile_num]->cacheKey(); }
};

#endif 
