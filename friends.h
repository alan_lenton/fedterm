/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#ifndef FRIENDS_H
#define FRIENDS_H

#include <list>
#include <string>

#include <QWidget>

#include "ui_friends.h"

class Friends : public QWidget
{
	Q_OBJECT

public:
	enum { LOGIN, LOGOUT };

private:
	static const int	MAX_RANKS = 16;
	static const std::string	rank_names[MAX_RANKS];

	Ui::Friends ui;

	std::list<std::string>	friends;	// Assumption: the list is not going to have more than 20 or so entries!
	bool	ranks_to_report[MAX_RANKS];
	QCheckBox	*rank_checks[MAX_RANKS];
	bool	audio_wanted;

	void	SaveFriends();
	void	SaveRanks();
	void	SetUpFriends();
	void	SetUpRanks();

private slots:
	void	AddName();
	void	AudioCheck();
	void	DeleteName();
	void	ClearAllFriends();
	void	ClearAllRanks();
	void	ListClicked(const QModelIndex& which);
	void	SaveAndExit();
	void	ScrapChanges();

public:
	Friends(QWidget *parent = 0);
	~Friends();

	void	Notify(std::string name,int rank,int notify_type);
};

#endif // FRIENDS_H
