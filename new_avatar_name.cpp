/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#include "new_avatar_name.h"

#include <cctype>
#include <string>

#include "fedterm.h"

NewAvatarName::NewAvatarName(QWidget *parent) : QDialog(parent)
{
	ui.setupUi(this);

	ui.name_edit->clear();

	connect(ui.retry_btn,SIGNAL(clicked()),this,SLOT(RetryName()));
	connect(ui.cancel_btn,SIGNAL(clicked()),this,SLOT(CancelConnection()));

}

NewAvatarName::~NewAvatarName()
{

}

bool NewAvatarName::AvatarOK()
{
	static const QString	title = "New Player";

	if(ui.name_edit->text().size() < 3)
	{
		FedTerm::MainWindow()->Message(title,"Avatar names must be at least\nthree characters long!");
		return false;
	}

	std::string text(ui.name_edit->text().toStdString());
	for(int count = 0;count < text.length();++count) 
	{
		if((std::isalpha(text[count]) == 0) || (text[count] == ' '))
		{
			FedTerm::MainWindow()->Message(title,"Avatar names must be all\nletters with no spaces!");
			return false;
		}
	}

	return true;
}

void	NewAvatarName::CancelConnection()
{
	FedTerm::MainWindow()->LogOff();
	hide();
}

void	NewAvatarName::RetryName()
{
	if(!AvatarOK())
		return;
	QString	text(ui.name_edit->text());
	text += '\n';
	FedTerm::MainWindow()->SendCmd(text);
	hide();
}

