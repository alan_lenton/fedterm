/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#include "xml_parser.h"

#include <sstream>
#include <cstdlib>


XmlParser::XmlParser()
{

}

XmlParser::~XmlParser()
{

}

void	XmlParser::BuildAttributes(const std::string& line)
{
	std::string::size_type	start = 0;
	std::string::size_type	end;
	attrib_list.clear();
	unsigned int len = line.length();
	if(len > 4)
		len -= 4;
	else	// need at least four chars for a valid element
		return;

	std::string	name, value;
	for(;;)
	{
		if(start >= len)
			return;

		end = line.find('=',start);
		if(end == std::string::npos)
			return;

		name = line.substr(start,end - start);
		start = end +2;end = line.find('\'',start);
		if(end == std::string::npos)
			return;

		value = line.substr(start,end - start);
		start = end + 2;
		UnEscapeXml(name);
		UnEscapeXml(value);
		attrib_list.push_back(std::make_pair(name,value));
	}
}

void	XmlParser::BuildElement(const std::string& line)
{
	std::string::size_type	end = line.find(' ');
	if(end == std::string::npos)	// no attributes...
	{
		element = line;
		attrib_list.clear();
	}
	else
	{
		element = line.substr(0,end);
		BuildAttributes(line.substr(end + 1));
	}
}

std::string&	XmlParser::EscapeXml(const std::string& text)
{
	static std::string	xml_text;

	std::ostringstream	buffer;
	auto	len = text.length();
	for(auto mark : text)
	{
		switch(mark)
		{
			case '<':	buffer << "&lt;";		break;
			case '>':	buffer << "&gt;";		break;
			case '&':	buffer << "&amp;";	break;
			case '\'':	buffer << "&apos;";	break;
			default:		buffer << mark;		break;

		}
	}
	xml_text =  buffer.str();
	return xml_text;
}

void	XmlParser::FindAttrib(const AttribList& attribs,const std::string& name,std::string& value)
{
	for(auto attrib : attribs)
	{
		if(attrib.first == name)
		{
			value = attrib.second;
			return;
		}
	}
	value = "";
}

bool	XmlParser::FindBoolAttrib(const AttribList& attribs,const std::string& name,bool default_val)
{
	std::string	temp;
	FindAttrib(attribs,name,temp);
	if(temp == "")
		return(default_val);
	else
		return(temp == "true");
}

int	XmlParser::FindNumAttrib(const AttribList& attribs,const std::string& name,int default_val)
{
	std::string	temp;
	FindAttrib(attribs,name,temp);
	if(temp == "")
		return default_val;
	else
		return(std::atoi(temp.c_str()));
}

void	XmlParser::Parse(const std::string& line)
{
	if(line.substr(0,5).find("<?xml") == std::string::npos)	//skip xml header line
		ProcessLine(line);
}

void	XmlParser::ProcessLine(const std::string& line)
{
	std::string::size_type	blanks = line.find_first_not_of("\t");
	if(blanks == std::string::npos)	// blank line
		return;

	std::string::size_type	start;
	if(line[blanks] == '<')
		start = line.find('<');
	else
		start = std::string::npos;
	std::string::size_type	end = line.find('>');
	if((start != std::string::npos) && (end != std::string::npos))
	{
		std::string	element_str(line.substr(start + 1, end - start - 1));
		if(element_str[0] == '/')	// is it an end element?
		{
			EndElement(element_str.substr(1));
			return;
		}

		// is it an element with attribs but no text data?
		if(element_str[element_str.length() - 1] == '/')
		{
			BuildElement(element_str.substr(0,element_str.length()- 1));
			StartElement(element,attrib_list);
			EndElement(element);
			return;
		}

		// looks like it has text data...
		BuildElement(element_str.substr(0,element_str.length()));
		StartElement(element,attrib_list);

		// look for the the text data
		if((end + 1) >= line.length())
			return;
		std::string	line2(line.substr(end + 1));
		std::string::size_type	end2 = line2.find("</");
		if(end2 == std::string::npos)
		{
			UnEscapeXml(line2);
			TextData(line2);
			return;
		}
		else
		{
			std::string	line4(line2.substr(0,end2));
			UnEscapeXml(line4);
			TextData(line4);
			std::string	line3(line2.substr(end2 + 2));
			std::string::size_type	end3 = line3.find('>');
			if(end2 != std::string::npos)
				EndElement(line3.substr(0,end3));
		}
	}
	else
	{
		if(end == std::string::npos)
		{
			std::string	the_line(line);
			UnEscapeXml(the_line);
			TextData(the_line);
		}
		else
		{
			std::string::size_type	start4 = line.find('<');
			std::string::size_type	end4 = line.find('>');
			if((start4 != std::string::npos) && (end4 != std::string::npos))
			{
				std::string	line5(line.substr(0,start4));
				UnEscapeXml(line5);
				TextData(line5);
				EndElement(line.substr(start4,end4 - start4));
			}
			else
			{
				std::string	the_line(line);
				UnEscapeXml(the_line);
				TextData(the_line);
			}
		}
	}
}

std::string&	XmlParser::UnEscapeXml(std::string& text)
{
	std::ostringstream	buffer;
	int	len = text.length();
	for(auto count = 0;count < len;count++)
	{
		if(text[count] == '&')
		{
			switch(text[count + 1])
			{
				case 'a':
					if(text[count + 2] == 'm')		{ buffer << '&'; count += 4; break;	}
					if(text[count + 2] == 'p')		{ buffer << '\''; count += 5; }
					break;
				case 'g':	buffer << '>'; count += 3;		break;
				case 'l':	buffer << '<'; count += 3;		break;
				case 'q':	buffer << '\"'; count += 5;	break; 
			}
		}
		else
			buffer << text[count];
	}
	text = buffer.str();
	return text;
}


/* -------------------- Work in progress -------------------- */


