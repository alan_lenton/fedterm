/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#ifndef FEDMAP_H
#define FEDMAP_H

#include <map>
#include <memory>
#include <string>

#include "xml_parser.h"

class		Location;
class		MapScene;	

using LocIndex = std::map<int,Location *>;

class FedMap
{
public:
	static const int	HEIGHT = 64;
	static const int	WIDTH = 64;

private:
	std::string	star_name;
	std::string	name;
	std::string	owner;
	LocIndex		loc_index;
	MapScene		*scene;
	int			current_loc_no;

	void	AddMvtArrows(int from,int direction);
	void	DisplayMapInfo();
	void	LoadNewMap();
	void	MakeFileName(std::string& file_name);
	void	MoveCurrentMarker(int new_current_loc);

public:
	FedMap();
	~FedMap();

	int	FindMvtTile(int from, int direction);
	
	void	NewLoc(const AttribList& attribs);
	void	AddLoc(Location *location);
	void	NewMap(const AttribList& attribs);
	void	SetScene(MapScene *map_scene)			{ scene =  map_scene; }
	void	UpdateTile(Location *loc);
	void	Write();
};

#endif
