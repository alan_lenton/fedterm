/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#ifndef STATS_H
#define STATS_H

#include <list>
#include <string>

using AttribList = std::list<std::pair<std::string,std::string>>;

class Stats
{
protected:
	static const int	UNKNOWN_VAL = -1;

	void	MakeBiValueStat(const AttribList& attribs,std::string &text);

public:
	// Row info for the stats table
	static const int	PERSONAL_START = 0;
	static const int	PERSONAL_SIZE = 7;
	static const int	SHIP_START = PERSONAL_START + PERSONAL_SIZE + 1;
	static const int	SHIP_SIZE = 8;
	static const int	COMPUTER_START = SHIP_START + SHIP_SIZE + 1;
	static const int	COMPUTER_SIZE = 5;
	static const int	WEAPON_START = COMPUTER_START + COMPUTER_SIZE + 1;
	static const int	WEAPON_SIZE = 7;

	Stats()	{	}
	virtual ~Stats()	{	}
};


#endif
