/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#include "exchange.h"

#include <sstream>

#include <cctype>

#include "fedterm.h"
#include "server_parser.h"

// NOTE: Also includes section header names
const std::string Exchange::commodity_names[] =
{
	"AGRICULTURE","Cereals","Fruit","Furs","Hides","Livestock","Meats","Soya","Spices","Textiles","Woods",
	"MINING","Alloys","Clays","Crystals","Gold","Monopoles","Nickel","Petrochemicals","Radioactives","Semiconductors","Xmetals",
	"INDUSTRIAL","Explosives","Generators","LanzariK","LubOils","Mechparts","Munitions","Pharmaceuticals","Polymers","Propellants","RNA","Nitros",
	"TECHNICAL","AntiMatter","Controllers","Droids","Electros","GAsChips","Lasers","NanoFabrics","Nanos","Powerpacks","Synths","Tools","TQuarks","Vidicasters","Weapons",
	"BIOLOGICAL","BioChips","BioComponents","Clinics","Laboratories","MicroScalpels","Probes","Proteins","Sensors","ToxicMunchers","Tracers",
	"LEISURE","Artifacts","Firewalls","Games","Holos","Hypnotapes","Katydidics","Libraries","Musiks","Sensamps","Simulations","Studios","Univators",
	""
};

Exchange::Exchange(const AttribList& attribs,ServerParser *parser)
{
	FedTerm::MainWindow()->ClearExchangeDisplay();
	parser->FindAttrib(attribs,"name",name);
	FedTerm::MainWindow()->AddExchangeTitle(name + " Commodity Exchange");
	FedTerm::MainWindow()->SendCmd("<c-send-manifest>\n");

}


void	Exchange::CargoLine(const AttribList& attribs,ServerParser *parser)
{
	std::string	name;
	XmlParser::FindAttrib(attribs,"name",name);
	if(name == "")
		return;

	int cost = XmlParser::FindNumAttrib(attribs,"cost",-1);
	if(cost < 0)
		return;

	std::string	origin;
	XmlParser::FindAttrib(attribs,"origin",origin);
	if(origin == "")
		return;

	std::ostringstream	buffer;
	buffer << name << ": " << cost << "ig/ton [Origin: " << origin << "]";
	FedTerm::MainWindow()->DisplayCargoLine(buffer.str());
}

void	Exchange::CargoManifest(const AttribList& attribs,ServerParser *parser)
{
	std::string	max_hold;
	parser->FindAttrib(attribs,"max-hold",max_hold);
	if(max_hold == "")
		return;

	std::string	cur_hold;
	parser->FindAttrib(attribs,"cur-hold",cur_hold);
	if(cur_hold == "")
		return;
	
	std::ostringstream	buffer;
	buffer << "Max cargo: " << max_hold << "\nSpace left:" << cur_hold;
	FedTerm::MainWindow()->DisplayManifest(buffer.str());
}

int Exchange::FindCurrentRow()
{
	auto	row = NO_INDEX;
	for(int count = 0;commodity_names[count] != "";++count)
	{
		if(commodity_names[count] == commod_name)
		{
			row = count;
			break;
		}
	}
	return row;
}

void	Exchange::Transaction(const AttribList& attribs,ServerParser *parser,int type)
{
	parser->FindAttrib(attribs,"name",commod_name);
	parser->FindAttrib(attribs,"stock",commod_stock);
	parser->FindAttrib(attribs,"price",commod_price);

	std::string	file_name(commod_name);
	for(auto c: file_name)
		c = std::tolower(c);
	FedTerm::MainWindow()->UpdateCommodityPic(file_name);
	FedTerm::MainWindow()->UpdateCommodityName(commod_name);


	std::string trn_type;
	std::string	ticker_type;
	if(type == BUY)
	{
		trn_type = "Exch buys at ";
		ticker_type = "+++Exchange buys ";
		FedTerm::MainWindow()->UpdateCommodityQuantity("");
	}
	else
	{
		trn_type = "Exch sells at ";
		ticker_type = "+++Exchange sells ";
		FedTerm::MainWindow()->UpdateCommodityQuantity(commod_stock + " tons available");
	}

	FedTerm::MainWindow()->UpdateCommodityPrice(trn_type + commod_price + "/ton");
	std::ostringstream	buffer;
	buffer << ticker_type << commod_name << " at " << commod_price << "/ton";
	if(type == SELL)
		buffer << " (" << commod_stock << ")";
	buffer << "+++                                                                        ";
	std::string	ticker(buffer.str());
	FedTerm::MainWindow()->StartCommodityTicker(ticker);

	int row = FindCurrentRow();
	FedTerm::MainWindow()->UpdateExchangeLine(row,(type == BUY) ? "Exch Buys" : "Exch Sells",commod_stock,commod_price);

	std::string	main_display(buffer.str());
	main_display.insert(3," ");
	main_display.insert(3,name);

	if(FedTerm::MainWindow()->WantsExchCopy())
		FedTerm::MainWindow()->DisplayMain(main_display);
	else
	{
		if(old_commod_name == commod_name)
		{
			FedTerm::MainWindow()->DisplayMain(old_display);
			FedTerm::MainWindow()->DisplayMain(main_display);
		}
		old_display = main_display;
		old_commod_name = commod_name;
	}
}

