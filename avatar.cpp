/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#include "avatar.h"

#include <sstream>

#include <cstdlib>

#include "fedterm.h"
#include "stat_table.h"
#include "xml_parser.h"

Avatar::Avatar()
{
	name = rank = "";
	stat_table = new StatTable();
}

Avatar::~Avatar()
{
	delete weapon_stats;

	delete stat_table;
}

void	Avatar::AvatarInfo(const AttribList& attribs)
{
	if(attribs.size() == 0)
		return;

	auto iter = attribs.begin();
	
	if((*iter).first == "stat")
		stat_table->ProcessAvatarInfo(attribs);
	else
	{
		if((*iter).first == "name")
			name = (*iter).second;
		else
		{
			if((*iter).first == "rank")
				rank = (*iter).second;
		}
		
		if((name != "") && (rank != ""))
		{
			std::ostringstream	buffer;
			buffer << "FedTerm version " << FedTerm::version;
			FedTerm::MainWindow()->SetTitle(buffer.str());
			buffer.str("");
			buffer << "- " << rank << " " << name << " -";
			FedTerm::MainWindow()->SetAvatarName(buffer.str());
			FedTerm::MainWindow()->UpdateAvatarPic(name);
		}
	}
}

void	Avatar::ComputerInfo(const AttribList& attribs)	{ stat_table->ProcessComputerInfo(attribs); }
void	Avatar::FullStats(const AttribList& attribs)		{ stat_table->FullUpdate(attribs); }
void	Avatar::ShipInfo(const AttribList& attribs)		{ stat_table->ProcessShipInfo(attribs); }
void	Avatar::Weapons(const AttribList& attribs)		{ stat_table->WeaponsUpdate(attribs); }
void	Avatar::WeaponInfo(const AttribList& attribs)	{ stat_table->ProcessWeaponsInfo(attribs); }
