/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#include "location.h"

#include <stdexcept>

#include <QString>
#include "fedterm.h"

const std::string	Location::exit_names[] = 
	{ "n", "ne", "e", "se", "s", "sw", "w", "nw", "u", "d", "" };

Location::Location(const AttribList& attribs)
{
	loc_no = XmlParser::FindNumAttrib(attribs,"loc-num",INVALID_LOC);
	if(loc_no == INVALID_LOC)
		throw std::domain_error("Invalid location number");

	XmlParser::FindAttrib(attribs,"name",name);
	if(name == "")
		throw std::domain_error("No location name provided");

	for(int index = NORTH;index < MAX_EXITS;++index)
		exits[index] = XmlParser::FindNumAttrib(attribs,exit_names[index],INVALID_LOC);

	std::string	loc_flags;
	XmlParser::FindAttrib(attribs,"flags",loc_flags);
	if(loc_flags != "")
	{
		if(loc_flags.find('s') != std::string::npos)		
			flags.set(SPACE);
		if(loc_flags.find('l') != std::string::npos)		
			flags.set(LINK);
		if(loc_flags.find('p') != std::string::npos)		
			flags.set(PEACE);
	}
}

Location::Location(int loc_num,std::string& the_name)
{
	loc_no = loc_num;
	name = the_name;
}

ExitList Location::GetExits()
{
	ExitList	return_list(new std::list<std::pair<int,int>>);
	for(int count = NORTH;count < MAX_EXITS;++count)
	if(exits[count] != NO_EXIT)
	{
		std::pair<int,int> p(count,exits[count]);
		return_list->push_back(p);
	}
	return return_list;
}

// Sample XML output format:
//<s-new-loc loc-num='260' name='Chez Diesel' flags='slp' se='325' u='259' d='325' />

void	Location::Write(std::ofstream& file)
{
	static const std::string	xml_element("   <fed-loc ");

	file << "   <fed-loc loc-num='" << loc_no << "' name='" << XmlParser::EscapeXml(name) << "'";
	for(int count = NORTH;count < MAX_EXITS;++count)
	{
		if(exits[count] != NO_EXIT)
			file << " " << exit_names[count] << "='" << exits[count] << "'";
	}

	if(flags.any())
	{
		file << " flags='";
		if(flags.test(SPACE))		file << 's';
		if(flags.test(LINK))			file << 'l';
		if(flags.test(EXCHANGE))	file << 'e';
		if(flags.test(SHIPYARD))	file << 'y';
		if(flags.test(REPAIR))		file << 'r';
		if(flags.test(HOSPITAL))	file << 'h';
		if(flags.test(INSURE))		file << 'i';
		if(flags.test(PEACE))		file << 'p';
		if(flags.test(BAR))			file << 'b';
		if(flags.test(UNLIT))		file << 'u';
		if(flags.test(CUSTOM))		file << 'k';
		if(flags.test(TELEPORT))	file << 't';
		if(flags.test(COURIER))		file << 'c';
		if(flags.test(PICKUP))		file << 'a';
		file << "'";
	}
	file << " />\n";
}
