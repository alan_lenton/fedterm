/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#include "stat_table.h"

#include <sstream>

#include <QString>

#include "fedterm.h"
#include "weapons.h"
#include "xml_parser.h"


const std::string	StatTable::xml_av_stat_names[] = 
{ 
	"sta",	"str",	"dex",	"int",		"cash",
	"pts",	"hc",		"ak",		"trader",	"merchant",
	"" 
};
const std::string	StatTable::av_display_names[] =
{
	"Stamina",		"Strength",		"Dexterity",	"Intelligence",	"Cash",			
	"Points",		"Hauler Pts",	"Akaturi Pts",	"Trader Pts",		"Merchant Pts",
	""
};

const std::string	StatTable::xml_ship_stat_names[] = 
	{ "class", "fuel", "hull", "engines", "shields", "cargo", "customs-cert", "" }; 
const std::string	StatTable::ship_display_names[] = 
	{ "Class","Fuel", "Hull", "Engines", "Shields", "Cargo Space", "Customs", "" };

const std::string	StatTable::xml_comp_stat_names[] = 
	{ "computer", "sensors", "jammers", "navcomp", "" }; 
const std::string	StatTable::comp_display_names[] = 
	{ "Computer", "Sensors", "Jammers", "Navcomp", "" };


StatTable::StatTable()
{
	full_update = true;
	av_points = "";
	customs = "";
	sensors = "";
	jammers = "";
	weapons = new Weapons();
}

StatTable::~StatTable()
{
	delete weapons;
}


void	StatTable::DisplayLine(const std::string& stat,const std::string& value,int row)
{
	if(value == "")
		return;

	if(row == UNKNOWN_ROW)
		row = FedTerm::MainWindow()->FindStatRow(stat);

   if(row != UNKNOWN_ROW)
		FedTerm::MainWindow()->SetStat(stat,value,row);
	else
		EndFullUpdate();
		
}

void	StatTable::EndFullUpdate()
{
	FedTerm	*main_window = FedTerm::MainWindow();
	int	index = 0;
	
	main_window->SetStat("  ~ Personal ~","  ",index++);
	main_window->SetStat(av_display_names[STA],stamina,index++);
	main_window->SetStat(av_display_names[STR],strength,index++);
	main_window->SetStat(av_display_names[DEX],dexterity,index++);
	main_window->SetStat(av_display_names[INTEL],intel,index++);
	main_window->SetStat(av_display_names[CASH],cash,index++);
	if(av_points != "")
		main_window->SetStat(av_points_type,av_points,index++);

	if(ship_class != "")
	{
		main_window->SetStat("  ", "  ", index++);
		main_window->SetStat("  ~ Ship ~","  ",index++);
		main_window->SetStat(ship_display_names[CLASS],ship_class,index++);
		main_window->SetStat(ship_display_names[FUEL],fuel,index++);
		main_window->SetStat(ship_display_names[HULL],hull,index++);
		main_window->SetStat(ship_display_names[ENG],engines,index++);
		if(shields != "")
			main_window->SetStat(ship_display_names[SHIELDS],shields,index++);
		main_window->SetStat(ship_display_names[CARGO],cargo,index++);
		if(customs != "")
			main_window->SetStat(ship_display_names[CUSTOMS],customs,index++);
	}

	if(computer != "")
	{
		main_window->SetStat("  ", "  ", index++);
		main_window->SetStat("  ~ Computer ~","  ",index++);
		main_window->SetStat(comp_display_names[COMPUTER],computer,index++);
		if(sensors != "")
			main_window->SetStat(comp_display_names[SENSORS],sensors,index++);
		if(jammers != "")
			main_window->SetStat(comp_display_names[JAMMERS],jammers,index++);
		if(navcomp != "")
			main_window->SetStat(comp_display_names[NAVCOMP],navcomp,index++);
	}

	weapons->EndUpdate(index);
	full_update = false;
}

void	StatTable::FullUpdate(const AttribList& attribs)
{
	std::string	status;
	XmlParser::FindAttrib(attribs,"status",status);
	if(status == "begin")
		StartFullUpdate();
	if(status == "end")
		EndFullUpdate();
}

void	StatTable::MakeBiValueStat(const AttribList& attribs,std::string &text)
{
	int max = XmlParser::FindNumAttrib(attribs,"max",UNKNOWN_VAL);
	int current = XmlParser::FindNumAttrib(attribs,"cur",UNKNOWN_VAL);
	if((max == UNKNOWN_VAL) || (current == UNKNOWN_VAL))
	{
		text = "Unknown";
		return;
	}
	std::ostringstream	buffer;
	buffer << current << "/" << max;
	text = buffer.str();
}

void	StatTable::MakeCashStat(const AttribList& attribs,std::string& text)
{
	std::ostringstream	buffer;
	int value = XmlParser::FindNumAttrib(attribs,"amount",0);
	if(value >= 1000000000)
		buffer << (value/1000000000) << "Gig";
	else
	{
		if(value >= 1000000)
			buffer << (value/1000000) << "Mig";
		else
			buffer << value << "ig";
	}
	cash = buffer.str();								
}

void	StatTable::MakeRankStat(const AttribList& attribs,int which)
{
	std::string	amount;
	XmlParser::FindAttrib(attribs,"amount",amount);
	if(amount == "")
		return;
	av_points = amount;
}

void	StatTable::ProcessAvatarInfo(const AttribList& attribs)
{
	std::string	stat;
	XmlParser::FindAttrib(attribs,"stat",stat);
	if(stat == "")
		return;

	if(stat == "cash")
	{
		MakeCashStat(attribs,cash);
		if(!full_update)
			DisplayLine("cash",cash);		
		return;
	}

	int index = UNKNOWN_VAL;
	for(int count = 0;xml_av_stat_names[count] != "";++count)
	{
		if(xml_av_stat_names[count] == stat)
		{
			index = count;
			break;
		}
	}
	if(index == UNKNOWN_VAL)
		return;

	std::ostringstream	buffer;
	switch(index)
	{
		case STA:	MakeBiValueStat(attribs,stamina);	
						if(!full_update) 
							DisplayLine(av_display_names[index],stamina);	
						break;
		case STR:	MakeBiValueStat(attribs,strength);	
						if(!full_update)
							DisplayLine(av_display_names[index],strength);	
						break;
		case DEX:	MakeBiValueStat(attribs,dexterity);	
						if(!full_update)
							DisplayLine(av_display_names[index],dexterity);	
						break;
		case INTEL:	MakeBiValueStat(attribs,intel);
						if(!full_update)
							DisplayLine(av_display_names[index],intel);		
						break;
		case HC:
		case AK:
		case TRADER:
		case MERCHANT:	MakeRankStat(attribs,index);
							av_points_type = av_display_names[index];
							if(!full_update)
								DisplayLine(av_points_type,av_points);
							break;
	}
}

void	StatTable::ProcessComputerInfo(const AttribList& attribs)
{
	std::string	stat;
	XmlParser::FindAttrib(attribs,"stat",stat);

	if(stat == "navcomp")
	{
		navcomp = "Installed";
		if(!full_update)
			DisplayLine(comp_display_names[NAVCOMP],navcomp);
		return;
	}

	int	index = UNKNOWN_VAL;
	for(auto count = 0;xml_comp_stat_names[count] != "";++count)
	{
		if(xml_comp_stat_names[count] == stat)
		{
			index = count;
			break;
		}
	}
	if(index == UNKNOWN_VAL)
		return;

	switch(index)
	{
		case COMPUTER:	MakeBiValueStat(attribs,computer);
							if(!full_update)
								DisplayLine(comp_display_names[index],computer);
							break;
		case SENSORS:	XmlParser::FindAttrib(attribs,"cur",sensors);
							if(!full_update)
								DisplayLine(comp_display_names[index],sensors);
							break;
		case JAMMERS:	XmlParser::FindAttrib(attribs,"cur",jammers);
							if(!full_update)
								DisplayLine(comp_display_names[index],jammers);	
							break;
	}
}

void	StatTable::ProcessShipInfo(const AttribList& attribs)
{
	std::string	stat;
	XmlParser::FindAttrib(attribs,"class",stat);
	if(stat != "")
	{
		ship_class = stat;
		if(!full_update)
			DisplayLine(ship_display_names[CLASS],ship_class);	
		FedTerm::MainWindow()->UpdateShipPic(ship_class);
		return;
	}

	XmlParser::FindAttrib(attribs,"stat",stat);
	int	index = UNKNOWN_VAL;
	for(auto count = 0;xml_ship_stat_names[count] != "";++count)
	{
		if(xml_ship_stat_names[count] == stat)
		{
			index = count;
			break;
		}
	}
	if(index == UNKNOWN_VAL)
		return;

	switch(index)
	{
		case FUEL:		MakeBiValueStat(attribs,fuel);
							if(!full_update)
								DisplayLine(ship_display_names[index],fuel);		
							break;
		case HULL:		MakeBiValueStat(attribs,hull);
							if(!full_update)
								DisplayLine(ship_display_names[index],hull);		
							break;
		case ENG:		MakeBiValueStat(attribs,engines);
							if(!full_update)
								DisplayLine(ship_display_names[index],engines);	
							break;
		case SHIELDS:	MakeBiValueStat(attribs,shields);	
							if(!full_update)
								DisplayLine(ship_display_names[index],shields);
							break;
		case CARGO:		MakeBiValueStat(attribs,cargo);
							if(!full_update)
								DisplayLine(ship_display_names[index],cargo);
							break;
		case CUSTOMS:	customs = "Certified";					
							if(!full_update)
								DisplayLine(ship_display_names[index],customs);
							break;
	}
}

void	StatTable::ProcessWeaponsInfo(const AttribList& attribs)
{
	weapons->ProcessInfo(attribs);
}

void	StatTable::StartFullUpdate()
{
	FedTerm::MainWindow()->ClearStatsPanel();
	weapons->Clear();
	full_update = true;
}

void	StatTable::WeaponsUpdate(const AttribList& attribs)
{
	std::string	status;
	XmlParser::FindAttrib(attribs,"status",status);
	if(status == "begin")
		weapons->StartUpdate();
	if(status == "end")
	{
		if(!full_update)
			weapons->EndUpdate(Weapons::CURRENT_START);
		else
			weapons->EndUpdate(Weapons::WRITE_LATER);
	}
}


/* ----------------- Work in Progress ----------------- */




