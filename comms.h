/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#ifndef COMMS_H
#define COMMS_H

#include<string>

#include <QObject>
#include <QTcpSocket>

class FedMap;
class FedTerm;
class LoginRec;
class NewbieRec;
class ServerParser;
class StatTable;

class Comms : public QObject
{
	Q_OBJECT

private:
	const static int	SERVER_BUFFER_SIZE = 2048;

	enum class Status   
	{ 
		NO_CONNECT, LOGIN, START_XML, WAIT_XML, PLAY, 
		NEWBIE_LOGIN
	};

	QTcpSocket		*tcp_socket;
	ServerParser	*parser;

	LoginRec		*login_rec;
	NewbieRec	*newbie_rec;
	Status		status;

	char *server_buffer;
	bool	name_taken;

	void	Login(std::string& buffer);
	void	NewbieLogin(std::string& buffer);
	void	StartXml(std::string& buffer);
	void	WaitXml(std::string& buffer);

private slots:
	void	CommsError(QAbstractSocket::SocketError error);
	void	ConnectionClosed();
	void	Connected();
	void	ParseIncoming();

public:
	Comms(QObject *parent);
	~Comms();

	void	ConnectToServer(LoginRec *rec);
	void	ConnectToServer(NewbieRec *rec);
	void	LogOff()			{ tcp_socket->disconnectFromHost(); }
	void	ShowFriends();
	void	WantTextAlerts(bool which);
	void	Write(const QString& text);
	void	Write(const char *text);
};

#endif 

