/*-----------------------------------------------------------------------
                FedTerm - Windows Federation 2 Terminal
                  Copyright (c) 1985-2016 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 2 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net/alan
-----------------------------------------------------------------------*/

#include "fed_map.h"

#include <fstream>
#include <sstream>

#include <cctype>

#include "fedterm.h"
#include "location.h"
#include "map_parser.h"
#include "map_scene.h"
#include "tiles.h"

FedMap::FedMap()
{
	scene = nullptr;
	current_loc_no = Location::INVALID_LOC;
}

FedMap::~FedMap()
{	
	Write();
}


void	FedMap::AddLoc(Location *loc)
{
	loc_index[loc->Number()] = loc;
	UpdateTile(loc);
}

void	FedMap::AddMvtArrows(int from,int direction)
{
	int	tile = FindMvtTile(from,direction);
	switch(direction)
	{
		case Location::NORTH:	scene->AddTile((from%HEIGHT)*2,((from/WIDTH)*2) - 1,tile);				break;
		case Location::NE:		scene->AddTile(((from % HEIGHT)*2) + 1,((from/WIDTH)*2) - 1,tile);	break;
		case Location::EAST:		scene->AddTile(((from % HEIGHT)*2) + 1,(from/WIDTH)*2,tile);			break;
		case Location::SE:		scene->AddTile(((from % HEIGHT)*2) + 1,((from/WIDTH)*2) + 1,tile);	break;
		case Location::SOUTH:	scene->AddTile((from % HEIGHT)*2,((from/WIDTH)*2) + 1,tile);			break;
		case Location::SW:		scene->AddTile(((from % HEIGHT)*2) - 1,((from/WIDTH)*2) + 1,tile);	break;
		case Location::WEST:		scene->AddTile(((from % HEIGHT)*2) - 1,(from/WIDTH)*2,tile);			break;
		case Location::NW:		scene->AddTile(((from % HEIGHT)*2) - 1,((from/WIDTH)*2) - 1,tile);	break;
	}
}

void	FedMap::DisplayMapInfo()
{
	std::ostringstream	buffer;
	buffer << name << " - " << star_name << " system - owned by " << owner;
	FedTerm::MainWindow()->SetInfoBar(buffer.str());
}

int	FedMap::FindMvtTile(int from, int direction)
{
	switch(direction)
	{
		case Location::NORTH:	return Tiles::NORTH;
		case Location::NE:		return Tiles::NE;
		case Location::EAST:		return Tiles::EAST;
		case Location::SE:		return Tiles::SE;
		case Location::SOUTH:	return Tiles::SOUTH;
		case Location::SW:		return Tiles::SW;
		case Location::WEST:		return Tiles::WEST;
		case Location::NW:		return Tiles::NW;
		default:						return Tiles::NO_TILE;
	}
}

void	FedMap::LoadNewMap()
{
	std::string f_name;
	MakeFileName(f_name);
	std::string	map_dir = FedTerm::base_dir + "/maps/";
	std::string file_name = map_dir + f_name;
	MapParser map_parser(file_name,this);
	map_parser.Parse();	// If there isn't a file yet, the parser will handle it
}

// File name is the map name with spaces replaced by underscores and all lower case 
void	FedMap::MakeFileName(std::string& file_name)
{
	file_name = name;
	for(auto& letter: file_name)
	{
		if(letter == ' ')
			letter = '_';
		if(std::isupper(letter))
			letter = std::tolower(letter);
	}
	if(file_name.size() > 18)
		file_name.erase(18);
	file_name += ".xml";
}

void	FedMap::MoveCurrentMarker(int new_current_loc_no)
{
	QGraphicsItem	*item;
	if(current_loc_no != Location::INVALID_LOC)
		item = scene->RemoveCurrentMarker((current_loc_no%HEIGHT)*2,(current_loc_no/WIDTH)*2);
	else
		item = nullptr;
	if(new_current_loc_no != Location::INVALID_LOC)
			scene->AddCurrentMarker((new_current_loc_no%HEIGHT)*2,(new_current_loc_no/WIDTH)*2,item);
	current_loc_no = new_current_loc_no;
}

void	FedMap::NewLoc(const AttribList& attribs)
{
	auto loc_no = XmlParser::FindNumAttrib(attribs,"loc-num",Location::INVALID_LOC);
	LocIndex::iterator iter = loc_index.find(loc_no);
	if(iter != loc_index.end())
	{
		delete iter->second;
		loc_index.erase(iter);
	}

	auto loc = new Location(attribs);
	loc_index[loc->Number()] = loc;
	UpdateTile(loc);
	
	MoveCurrentMarker(loc_no);
	std::string loc_name;
	XmlParser::FindAttrib(attribs,"name",loc_name);
	if(loc_name != "")
	{
		FedTerm::MainWindow()->DisplayMain(loc_name);
		FedTerm::MainWindow()->SetLocInfo(loc_name);
	}
}

void	FedMap::NewMap(const AttribList& attribs)
{
	if(!loc_index.empty())
	{
		Write();
		for(auto iter = loc_index.begin(); iter != loc_index.end();++iter)
			delete iter->second;
		loc_index.clear();
	}

	XmlParser::FindAttrib(attribs,"system",star_name);
	XmlParser::FindAttrib(attribs,"name",name);
	XmlParser::FindAttrib(attribs,"owner",owner);

	scene->InitialiseMap();
	LoadNewMap();
	DisplayMapInfo();
}

void	FedMap::UpdateTile(Location *loc)
{
	auto	exits = loc->GetExits();
	int	tile = Tiles::USED;
	bool	hidden_exits = false;
	int	loc_no = loc->Number();
	if(!exits->empty())
	{
		for(auto iter = exits->begin();iter != exits->end();++iter)
		{
			switch((*iter).first)
			{
				case Location::NORTH:
				case Location::NE:
				case Location::EAST:
				case Location::SE:
				case Location::SOUTH:
				case Location::SW:
				case Location::WEST:		// Drop through
				case Location::NW:	AddMvtArrows(loc_no,(*iter).first);	break;
				case Location::UP:
				case Location::DOWN:	tile = Tiles::HIDDEN;
											hidden_exits = true;
											break;
				default:					tile = Tiles::USED;	break;
			}
		}
	}
	if(hidden_exits)
		tile = Tiles::HIDDEN;
	if(loc->IsPeace())
		tile = Tiles::PEACE;
	if(loc->IsLink())
		tile = Tiles::LINK;
	scene->ChangeTile((loc_no%HEIGHT)*2,(loc_no/WIDTH)*2,tile,loc->Name());
}

void	FedMap::Write()
{
	if(loc_index.empty())
		return;

	std::string	f_name;
	MakeFileName(f_name);
	std::string	map_dir = FedTerm::base_dir + "/maps/";
	if(!FedTerm::MainWindow()->MakeDirectory(map_dir))
	{
		FedTerm::MainWindow()->Message(QString("Error"),QString("Unable to create directory for map files"));
		return;
	}

	std::string	file_name = map_dir + f_name;
	std::ofstream	file(file_name,std::ios::out);
	if(!file)
	{
		FedTerm::MainWindow()->Message(QString::fromStdString(f_name), "Unable to open file to save map");
		return;
	}

	file << "<fed-map  system='" << XmlParser::EscapeXml(star_name) << "' name='" << XmlParser::EscapeXml(name) << "' owner='" << XmlParser::EscapeXml(owner) << "' >\n";
	for(auto loc: loc_index)
		loc.second->Write(file);
	file << "</fed-map>\n";
}

